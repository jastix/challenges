defmodule UppercaseString do
  # check if the string is upper cased.
  def upper_case?(""), do: true
  def upper_case?(str), do: _upper_case?(String.to_char_list(str))

  defp _upper_case?([h | _t]) when h in ?a..?z do
    false
  end

  defp _upper_case?([_h | t]) do
    _upper_case?(t)
  end

  defp _upper_case?([]), do: true
end
