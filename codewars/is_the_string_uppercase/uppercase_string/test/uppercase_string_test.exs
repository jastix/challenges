defmodule UppercaseStringTest do
  use ExUnit.Case
  doctest UppercaseString

  test "it returns True for an empty string" do
    assert UppercaseString.upper_case?("") == true
  end

  test "it returns False for downcased character" do
    assert UppercaseString.upper_case?("c") == false
  end

  test "it returns True for uppercased character" do
    assert UppercaseString.upper_case?("C") == true
  end

  test "it returns False if there are downcased characters in a string" do
    assert UppercaseString.upper_case?("hello I AM DONALD") == false
  end

  test "it returns True for all uppercased string" do
    assert UppercaseString.upper_case?("HELLO I AM DONALD") == true
  end

  test "it returns false if there is a downcased character in a string" do
    assert UppercaseString.upper_case?("ACSKLDFJSgSKLDFJSKLDFJ") == false
  end

  test "it returns True if for uppercased string without spaces" do
    assert UppercaseString.upper_case?("ACSKLDFJSGSKLDFJSKLDFJ") == true
  end
end
