# UppercaseString

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `uppercase_string` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:uppercase_string, "~> 0.1.0"}]
    end
    ```

  2. Ensure `uppercase_string` is started before your application:

    ```elixir
    def application do
      [applications: [:uppercase_string]]
    end
    ```

