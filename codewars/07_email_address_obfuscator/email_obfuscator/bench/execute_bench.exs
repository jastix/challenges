defmodule ExecuteBench do
  use Benchfella
  import EmailObfuscator

  @string "hello.world.elixir.is.coming@elixir.world.domination.is.inevitable.com"

  bench "with pattern matching" do
    execute @string
  end

  bench "with String.replace" do
    execute_replace @string
  end
end
