defmodule EmailObfuscatorTest do
  use ExUnit.Case
  import EmailObfuscator
  doctest EmailObfuscator

  test "swaps @ and . characters" do
    assert execute("test@mail.com") == "test [at] mail [dot] com"
    assert execute_replace("test@mail.com") == "test [at] mail [dot] com"
  end

  test "swaps several . characters" do
    assert execute("jon.snow.1990@mail.com") == "jon [dot] snow [dot] 1990 [at] mail [dot] com"
    assert execute_replace("jon.snow.1990@mail.com") == "jon [dot] snow [dot] 1990 [at] mail [dot] com"
  end
end
