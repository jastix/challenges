defmodule EmailObfuscator do

  # solution with pattern matching
  def execute(email) do
    email
    |> String.codepoints
    |> _execute
  end

  defp _execute(list, result \\ [])
  defp _execute([h | t], result) when h == "@" do
    _execute(t, [" [at] "] ++ result)
  end

  defp _execute([h | t], result) when h == "." do
    _execute(t, [" [dot] "] ++ result)
  end

  defp _execute([h | t], result), do: _execute(t, [h] ++ result)
  defp _execute([], result), do: result |> Enum.reverse |> List.to_string

  # solution that uses String.replace/3
  def execute_replace(email) do
    email
    |> String.replace(".", " [dot] ")
    |> String.replace("@", " [at] ")
  end
end
