# EmailObfuscator

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `email_obfuscator` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:email_obfuscator, "~> 0.1.0"}]
    end
    ```

  2. Ensure `email_obfuscator` is started before your application:

    ```elixir
    def application do
      [applications: [:email_obfuscator]]
    end
    ```

