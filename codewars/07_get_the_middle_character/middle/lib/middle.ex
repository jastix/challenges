defmodule Middle do
  require Integer
  @moduledoc """
  Given a word return a middle element of the word.
  If the word is even then return two middle characters of the word.
  If the word is odd then return one middle character of the word.
  """

  def middle(""), do: ""
  def middle(str) do
    str_length = String.length(str)
    start_idx = str_length |> div(2)
    case str_length |> Integer.is_even do
      true  -> String.slice(str, start_idx - 1, 2)
      false -> String.at(str, start_idx)
    end
  end
end
