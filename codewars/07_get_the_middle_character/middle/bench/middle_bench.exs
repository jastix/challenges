defmodule MiddleBench do
  use Benchfella

  @str Enum.map(?a..?z, fn(x) -> <<x :: utf8>> end) |> List.to_string

  bench "return middle element" do
    Middle.middle(@str)
  end
end
