defmodule MiddleTest do
  use ExUnit.Case

  test "returns empty string when empty string is given" do
    assert Middle.middle("") == ""
  end

  test "returns the same string if the given string of one char" do
    assert Middle.middle("A") == "A"
  end

  test "returns the same string if the given string of one char (russian)" do
    assert Middle.middle("ё") == "ё"
  end

  test "returns the same string if the given string of one char (unicode)" do
    assert Middle.middle("ł") == "ł"
  end

  test "returns middle characters from even string" do
    assert Middle.middle("test") == "es"
  end

  test "returns middle character from odd string" do
    assert Middle.middle("testing") == "t"
  end

end
