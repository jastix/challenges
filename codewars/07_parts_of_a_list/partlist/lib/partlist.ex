defmodule Partlist do
  def part_list([]),   do: []
  def part_list(list), do: _part_list(list, [], Enum.count(list) - 1)

  defp _part_list(_list, acc, 0), do: acc |> Enum.reverse
  defp _part_list(list, acc, counter) do
    new_el =
      Enum.split(list, counter)
      |> _format_parts

    _part_list(list, acc ++ new_el, counter - 1)
  end

  defp _format_parts({h, t}) do
    [[Enum.join(h, " "), Enum.join(t, " ")]]
  end
  # defp _part_list([h | []], acc) do
  #   acc
  # end
  # defp _part_list([h | t], []) do
  #   _part_list(t, [[ h, Enum.join(t, " ") ]])
  # end

  # defp _part_list([h | t], [[ha, hb] | ta]) do
  #   _part_list(t, [[ ha, hb]] ++ [ [ Enum.join([(ha), h], " ") ,
  #                          Enum.join(t, " ")]])
  # end
end
