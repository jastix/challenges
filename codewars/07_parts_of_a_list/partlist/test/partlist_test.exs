defmodule PartlistTest do
  use ExUnit.Case
  import Partlist
  doctest Partlist

  test "it splits a string into parts" do
    assert part_list(["Hello", "green", "world"]) == [
      ["Hello", "green world"],
      ["Hello green", "world"]
    ]
  end
  
  test "it splits another list" do
    assert part_list(["I", "wish", "I", "hadn't", "come"]) ==
      [
        ["I", "wish I hadn't come"],
        ["I wish", "I hadn't come"],
        ["I wish I", "hadn't come"],
        ["I wish I hadn't", "come"]
      ]
  end

  test " it splits a list with strange data" do
    assert part_list(["cdIw", "tzIy", "xDu", "rThG"]) ==
      [["cdIw", "tzIy xDu rThG"], ["cdIw tzIy", "xDu rThG"], ["cdIw tzIy xDu", "rThG"]]

    assert part_list(["vJQ", "anj", "mQDq", "sOZ"]) ==
      [["vJQ", "anj mQDq sOZ"], ["vJQ anj", "mQDq sOZ"], ["vJQ anj mQDq", "sOZ"]]
  end
end
