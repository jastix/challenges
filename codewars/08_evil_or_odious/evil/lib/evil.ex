defmodule Evil do
  # evil numbers have even number of 1's in binary
  # odious numbers have odd number of 1's in binary
  def evil?(n) do
    n
    |> Integer.digits(2)
    |> Enum.filter(fn(x) -> x == 1 end)
    |> Enum.count
    |> _evil_or_odious
  end

  defp _evil_or_odious(n) do
    case rem(n, 2) do
      0 ->
        "It's Evil!"
      _ ->
        "It's Odious!"
    end
  end
end
