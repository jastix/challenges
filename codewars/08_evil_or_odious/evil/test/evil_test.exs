defmodule EvilTest do
  use ExUnit.Case
  import Evil
  doctest Evil

  test "is odious" do
    assert evil?(1) == "It's Odious!"
    assert evil?(2) == "It's Odious!"
  end

  test "is evil" do
    assert evil?(3) == "It's Evil!"
  end
end
