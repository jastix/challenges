defmodule Finder do
  def find_needle(list), do: _find_needle(list, 0)

  defp _find_needle(["needle" | _t], pos), do: "found the needle at position #{pos}"
  defp _find_needle([_h | t], pos), do: _find_needle(t, pos + 1)
end
