defmodule BillTest do
  use ExUnit.Case
  import Bill
  doctest Bill

  test "returns differences for a bill" do
    assert split_the_bill(%{a: 30, b: 10}) == %{a: 10, b: -10}
  end

  test "it returns differences for a bill 2" do
    assert split_the_bill(%{A: 20, B: 15, C: 10}) == %{A: 5.00, B: 0.00, C: -5.00}
  end

  test "it returns differences for a bill 3" do
    assert split_the_bill(%{A: 40, B: 25, C: 10, D: 153, E: 58}) == %{A: -17.20, B: -32.20, C: -47.20, D: 95.80, E: 0.80}
  end

  test "it returns differences for a bill 4" do
    assert split_the_bill(%{A: 20348, B: 493045, C: 2948, D: 139847, E: 48937534, F: 1938724, G: 4, H: 2084}) ==
      %{A: -6421468.75, B: -5948771.75, C: -6438868.75, D: -6301969.75, E: 42495717.25, F: -4503092.75, G: -6441812.75, H: -6439732.75}
  end
end
