defmodule Bill do
  def split_the_bill(group) do
    group
    |> calc_average
    |> calc_diffs(group)
  end

  defp calc_average(group) do
    total = Map.values(group) |> Enum.sum
    total / Enum.count(group)
  end

  defp calc_diffs(avg, group) do
    group
    |> Enum.reduce(%{}, fn({k, v}, acc) ->
         Map.put(acc, k, Float.round(v - avg, 2))
       end)
  end
end
