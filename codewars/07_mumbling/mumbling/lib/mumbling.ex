defmodule Mumbling do
  def accum(string) do
    string
    |> String.codepoints
    |> Enum.with_index(1)
    |> Enum.map(fn({x, n}) -> String.duplicate(x, n) end)
    |> Enum.map(&String.capitalize(&1))
    |> Enum.join("-")
  end

  def accum_stream(string) do
    string
    |> String.codepoints
    |> Stream.with_index(1)
    |> Stream.map(fn({x, n}) -> String.duplicate(x, n) end)
    |> Stream.map(&String.capitalize(&1))
    |> Enum.join("-")
  end
end
