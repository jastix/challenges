defmodule ExecuteBench do
  use Benchfella
  import Mumbling

  @string ?a..?z |> Enum.to_list |> Enum.flat_map(&List.duplicate(&1, 100)) |> IO.chardata_to_string

  bench "with Enum" do
    accum @string
  end

  bench "with Stream" do
    accum_stream @string
  end
end
