# Mumbling

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `mumbling` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:mumbling, "~> 0.1.0"}]
    end
    ```

  2. Ensure `mumbling` is started before your application:

    ```elixir
    def application do
      [applications: [:mumbling]]
    end
    ```

