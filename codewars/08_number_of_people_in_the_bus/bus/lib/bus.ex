defmodule Bus do
  # find out how many people are on the bus after all stops
  def number([]),    do: 0
  def number(stops), do: _number(stops, 0)

  defp _number([{enter, exit} | t], count), do: _number(t, count + (enter - exit))
  defp _number([], count), do: count

  # using Enum.reduce
  def number_r(stops) do
    Enum.reduce(stops, 0, fn({enter, exit}, acc) -> acc + enter - exit end)
  end
end
