defmodule CodewarsZoo do
  def fix_the_meerkat({ tail, body, head }), do: { head, body, tail }
end
