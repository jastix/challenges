defmodule CountdupTest do
  use ExUnit.Case
  import Countdup
  doctest Countdup

  test "empty string" do
    assert count_dups("") == 0
  end

  test "non empty string without duplicates" do
    assert count_dups("123") == 0
  end

  test "non empty string with duplicates 1" do
    assert count_dups("1223") == 1
  end

  test "non empty string with duplicates 2" do
    assert count_dups("aabbcde") == 2
  end

  test "non empty string with duplicates 3" do
    assert count_dups("aabbcdeB") == 2
  end

  test "non empty string with duplicates 4" do
    assert count_dups("indivisibility") == 1
  end

  test "non empty string with duplicates 5" do
    assert count_dups("indivisibilities") == 2
  end

  test "non empty string with duplicates 6" do
    assert count_dups("aa11") == 2
  end

  test "non empty string with duplicates 7" do
    assert count_dups("aabBcde") == 2
  end
end
