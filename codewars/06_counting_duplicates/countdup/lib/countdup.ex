defmodule Countdup do
  @moduledoc """
  Documentation for Countdup.
  Write a function that will return the count of distinct case-insensitive
  alphabetic characters and numeric digits that occur more than once
  in the input string. The input string can be assumed to contain only
  alphanumeric characters, including digits, uppercase and lowercase alphabets.
  """

  def count_dups(""), do: 0

  def count_dups(str) do
    str
    |> String.downcase
    |> String.graphemes
    |> count_dups(%{})
  end

  defp count_dups([h | t], acc) do
    acc = Map.update(acc, h, 1, &(&1 + 1))
    count_dups(t, acc)
  end

  defp count_dups([], acc) do
    acc
    |> Map.to_list
    |> Enum.filter(fn({_k, v}) -> v > 1 end)
    |> Enum.count
  end
end
