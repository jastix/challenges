defmodule Minmax do
  # create functions min and max that return minimum or maximum elements
  # of a list, respectively.
  def min([]),   do: nil
  def min(list), do: _min(list)

  defp _min([h | []]), do: h
  defp _min([h | [a | t]]) when h > a, do: _min([a | t])
  defp _min([h | [_a | t]]), do: _min([h | t])

  def max([]),   do: nil
  def max(list), do: _max(list)

  defp _max([h | []]), do: h
  defp _max([h | [a | t]]) when h < a, do: _max([a | t])
  defp _max([h | [_a | t]]), do: _max([h | t])
end
