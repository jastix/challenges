defmodule MinmaxTest do
  use ExUnit.Case
  doctest Minmax

  test "the truth" do
    assert Minmax.min([]) == nil
    assert Minmax.max([]) == nil
  end

  test "it returns the same element for one-element list" do
    assert Minmax.min([1]) == 1
    assert Minmax.max([1]) == 1
  end

  test "it returns minimun element of a list" do
    assert Minmax.min([3,2,1]) == 1
    assert Minmax.min([-52, 56, 30, 29, -54, 0, -110]) == -110
    assert Minmax.min([42, 54, 65, 87, 0]) == 0
  end

  test "it returns maximum element of a list" do
    assert Minmax.max([1,3,2]) == 3
    assert Minmax.max([4,6,2,1,9,63,-134,566]) == 566
    assert Minmax.max([5]) == 5
  end
end
