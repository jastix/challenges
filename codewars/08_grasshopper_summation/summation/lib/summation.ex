defmodule Summation do
  def summation(n) do
    0..n
    |> Enum.to_list
    |> Enum.sum
  end
end
