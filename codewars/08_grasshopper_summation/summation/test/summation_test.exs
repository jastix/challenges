defmodule SummationTest do
  use ExUnit.Case
  import Summation
  doctest Summation

  test "it returns the correct total" do
    assert summation(0) == 0
    assert summation(1) == 1
    assert summation(8) == 36
  end
end
