# Summation

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `summation` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:summation, "~> 0.1.0"}]
    end
    ```

  2. Ensure `summation` is started before your application:

    ```elixir
    def application do
      [applications: [:summation]]
    end
    ```

