defmodule NextBigThing do
  # return an item that is after given item.
  # return nil if provided item is missing or is last element.
  def next_item([], _item), do: nil
  def next_item([item | [h | _t]], item), do: h
  def next_item([_h | t], item), do: next_item(t, item)
end
