# NextBigThing

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `next_big_thing` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:next_big_thing, "~> 0.1.0"}]
    end
    ```

  2. Ensure `next_big_thing` is started before your application:

    ```elixir
    def application do
      [applications: [:next_big_thing]]
    end
    ```

