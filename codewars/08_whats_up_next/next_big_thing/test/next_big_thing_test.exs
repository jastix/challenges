defmodule NextBigThingTest do
  use ExUnit.Case
  import NextBigThing
  doctest NextBigThing

  test "it returns nil when item is not present" do
    assert next_item([1,2,3,4], 5) == nil
  end

  test "it returns nil when nothing follows the item" do
    assert next_item(["a", "b", "c"], "c") == nil
  end

  test "it works with numbers" do
    assert next_item(Enum.to_list(1..100), 7) == 8
  end

  test "it works with string" do
    assert next_item(["Joe", "Bob", "Sally"], "Bob") == "Sally"
  end

  test "it works with charlist" do
    assert next_item('character list', ?l) == ?i
  end
end
