defmodule Detector do
  # is your_points higher than average for the whole class?
  def better_than_average(class_points, your_points) do
    class_points ++ [your_points]
    |> class_average
    |> is_better?(your_points)
  end

  def class_average(points) do
    Enum.sum(points) / Enum.count(points)
  end

  def is_better?(average, your_points) when your_points > average, do: true
  def is_better?(_average, _your_points), do: false
end
