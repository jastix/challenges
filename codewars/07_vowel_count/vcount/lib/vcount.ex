defmodule Vcount do
  @moduledoc """
  Returns the number (count) of vowels in the given string.

  We will consider a, e, i, o, and u as vowels.
  """

  @vowels ["a", "e", "i", "o", "u"]

  def count(""), do: 0
  def count(str) do
    str
    |> String.downcase()
    |> String.graphemes()
    |> Enum.filter(fn(x) -> x in @vowels end)
    |> Enum.count()
  end


  def count_recursive(str) do
    str
    |> String.downcase()
    |> String.graphemes()
    |> count_vowels(0)
  end

  defp count_vowels([], count), do: count
  defp count_vowels([h | t], count) when h in @vowels do
    count_vowels(t, count + 1)
  end
  defp count_vowels([_h | t], count), do: count_vowels(t, count)
end
