defmodule VcountTest do
  use ExUnit.Case
  doctest Vcount

  test "returns 0 for empty string" do
    assert Vcount.count("") == 0
  end

  test "counts 1 vowel" do
    assert Vcount.count("a") == 1
  end

  test "counts 2 vowels" do
    assert Vcount.count("hoi") == 2
  end

  test "counts 3 vowels" do
    assert Vcount.count("ArchImeD") == 3
  end

  test "counts 5 vowels" do
    assert Vcount.count("AEIOU") == 5
  end
end
