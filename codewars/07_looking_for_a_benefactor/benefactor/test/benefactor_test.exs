defmodule BenefactorTest do
  use ExUnit.Case
  import Benefactor
  doctest Benefactor

  test "it returns a missing average number" do
    assert new_avg([14, 30, 5, 7, 9, 11, 15], 30) == 149
    assert new_avg([14, 30, 5, 7, 9, 11, 16], 90) == 628
  end

  test "it raises an error" do
    assert_raise ArgumentError, "Expected New Average is too low", fn ->
      new_avg([14, 30, 5, 7, 9, 11, 15], 2)  == 0
    end
  end

  test "it rounds floats" do
    assert new_avg([1400.25, 30000.76, 5.56, 7, 9, 11, 15.48, 120.98], 10000) == 58430
  end
end
