defmodule Benefactor do
  def new_avg(nums, navg) do
    navg
    |> mulply_by_nums_count(nums)
    |> substitute_sum_of_nums(nums)
    |> round_result
    |> check_result
  end

  defp mulply_by_nums_count(navg, nums) do
    navg * (Enum.count(nums) + 1)
  end

  defp substitute_sum_of_nums(result, nums) do
    result - Enum.sum(nums)
  end

  defp round_result(result) do
    cond do
      is_float(result) ->
        result
        |> Float.ceil
        |> round
      is_integer(result) ->
        result
      true ->
        raise ArgumentError
    end
  end

  defp check_result(result) do
    cond do
      result >= 0 ->
        result
      true ->
        raise ArgumentError, "Expected New Average is too low"
    end
  end
end
