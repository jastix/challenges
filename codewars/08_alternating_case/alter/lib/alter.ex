defmodule Alter do
  def alter_case(string) do
    string
    |> String.to_char_list
    |> _alter_case([])
    |> Enum.reverse
    |> List.to_string
  end

  defp _alter_case([h | t], acc) when h in ?a..?z do
    _alter_case(t, [h - 32] ++ acc)
  end

  defp _alter_case([h | t], acc) when h in ?A..?Z do
    _alter_case(t, [h + 32] ++ acc)
  end

  defp _alter_case([h | t], acc), do: _alter_case(t, [h] ++ acc)

  defp _alter_case([], acc), do: acc
end
