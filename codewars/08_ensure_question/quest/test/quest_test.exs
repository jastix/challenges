defmodule QuestTest do
  use ExUnit.Case
  doctest Quest
  import Quest

  test "given empty string it returns a question mark" do
    assert ensure("") == "?"
  end

  test "adds question mark" do
    assert ensure("why") == "why?"
  end

  test "does not add additional question mark" do
    assert ensure("where?") == "where?"
  end
end
