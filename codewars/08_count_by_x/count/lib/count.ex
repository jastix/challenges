defmodule Count do
  def count_by(x, n) do
    _count_by(x, n) |> Enum.reverse
  end

  defp _count_by(_x, 0), do: []
  defp _count_by(x, n), do: [ x * n ] ++ _count_by(x, n - 1)
end
