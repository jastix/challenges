defmodule Opposite do
  # return opposite of a given number
  def opposite(num), do: -num
end
