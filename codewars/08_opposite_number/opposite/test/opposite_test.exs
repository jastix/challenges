defmodule OppositeTest do
  use ExUnit.Case
  import Opposite
  doctest Opposite

  test "the truth" do
    assert opposite(1) === -1
    assert opposite(0) === 0
    assert opposite(-34) === 34
  end
end
