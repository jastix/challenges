defmodule Countdigit do
  # count numbers of times digit d appears in 0..n each squared.
  def nb_dig(n, d) do
    n
    |> square_numbers
    |> count_digit(d)
  end

  defp square_numbers(n) do
    0..n
    |> Stream.map(fn(x) -> :math.pow(x, 2) |> round end)
    |> Enum.to_list
  end

  defp count_digit(nums, d) do
    nums
    |> Stream.flat_map(fn(x) -> Integer.digits(x) end)
    |> Enum.count(fn(x) -> x == d end)
  end
end
