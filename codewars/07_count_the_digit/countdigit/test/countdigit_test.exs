defmodule CountdigitTest do
  use ExUnit.Case
  import Countdigit
  doctest Countdigit

  test "the truth" do
    assert nb_dig(10, 1) == 4
    assert nb_dig(25, 1) == 11
  end

  test "counts digits" do
    assert nb_dig(5750, 0) == 4700
    assert nb_dig(11011, 2) == 9481
    assert nb_dig(12224, 8) == 7733
    assert nb_dig(11549, 1) == 11905
  end
end
