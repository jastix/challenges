# Reward

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `reward` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:reward, "~> 0.1.0"}]
    end
    ```

  2. Ensure `reward` is started before your application:

    ```elixir
    def application do
      [applications: [:reward]]
    end
    ```

