defmodule RewardTest do
  use ExUnit.Case
  import Reward
  doctest Reward

  test "salary with bonus" do
    assert bonus_time(100, true) == "$1000"
    assert bonus_time(2, true) == "$20"
    assert bonus_time(67890, true) == "$678900"
  end

  test "salary without bonus" do
    assert bonus_time(100, false) == "$100"
    assert bonus_time(78, false) == "$78"
    assert bonus_time(60000, false) == "$60000"
  end
end
