defmodule Reward do
  # if bonus is true then multiply salary by 10
  # otherwise return just salary
  def bonus_time(salary, bonus), do: "$#{_bonus_time(salary, bonus)}"

  defp _bonus_time(salary, true),  do: salary * 10
  defp _bonus_time(salary, false), do: salary

end
