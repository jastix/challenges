def sum_shared_answers(filename):
    sum_answers = 0
    with open(filename) as f:
        seen_answers = []
        for line in f:
            line = line.strip()
            if len(line) > 0:
                line_set = set(line)
                seen_answers.append(line_set)
            else:
                all_yes_answers = set.intersection(*seen_answers)
                sum_answers += len(all_yes_answers)
                seen_answers = []
        # last line processing
        if seen_answers:
            all_yes_answers = set.intersection(*seen_answers)
            sum_answers += len(all_yes_answers)
            seen_answers = []
    return sum_answers


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Total: {}".format(sum_shared_answers(FILENAME)))  # 3276
