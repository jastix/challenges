def sum_unique_answers(filename):
    sum_unique_answers = 0
    with open(filename) as f:
        seen_answers = set()
        for line in f:
            line = line.strip()
            if len(line) > 0:
                line_set = set(line)
                seen_answers.update(line_set)
            else:
                sum_unique_answers += len(seen_answers)
                seen_answers = set()
        # last line processing
        if seen_answers:
            sum_unique_answers += len(seen_answers)
            seen_answers = set()
    return sum_unique_answers


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Total: {}".format(sum_unique_answers(FILENAME)))  # 6585
