import linecache  # to read random lines from files


def compute_accumulator(filename):
    acc = 0
    seen_instr = set()
    current_idx = 1
    while True:
        line = linecache.getline(filename, current_idx).strip()
        op, val = line.split()
        if op == 'acc':
            acc += int(val)
            current_idx += 1
        elif op == 'nop':
            current_idx += 1
        else:
            current_idx += int(val)
        if current_idx in seen_instr:
            break
        seen_instr.add(current_idx)
    return acc


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Total: {}".format(compute_accumulator(FILENAME)))  # 1939
