import linecache  # to read random lines from files


def execute_code(filename, flip_op_num=None):
    acc = 0
    seen_instr = set()
    bad_instr = set()
    halting_state = False
    current_idx = 1
    while True:
        line = linecache.getline(filename, current_idx).strip()
        # empty line is when linecache returns non-existent string
        if line.strip() == "":
            halting_state = True
            break
        op, val = line.split()
        # change potentially bad instruction
        if flip_op_num == current_idx and op in ("jmp", "nop"):
            if op == "jmp":
                op = "nop"
            else:
                op = "jmp"
        if op == "acc":
            acc += int(val)
            current_idx += 1
        elif op == "nop":
            bad_instr.add(current_idx)
            current_idx += 1
        else:
            bad_instr.add(current_idx)
            current_idx += int(val)
        if current_idx in seen_instr:
            break
        seen_instr.add(current_idx)
    return (acc, bad_instr, halting_state)


def compute_accumulator(filename):
    acc = 0
    # collect all potentially bad instructions first
    _, bad_instr, _ = execute_code(filename)
    for instr in bad_instr:
        acc_execution, bad_instr, halting_state = execute_code(
            filename, flip_op_num=instr
        )
        if halting_state is True:
            acc = acc_execution
            break
    return acc


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Total: {}".format(compute_accumulator(FILENAME)))  # 2212
