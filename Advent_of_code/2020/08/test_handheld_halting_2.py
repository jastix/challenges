from handheld_halting_2 import compute_accumulator


def test_compute_accumulator():
    input_file = "input_test_1.txt"
    assert compute_accumulator(input_file) == 8
