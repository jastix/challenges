#!/usr/bin/env python
# -*- coding: utf-8 -*-
import functools

FILENAME = "input.txt"


def count_letter_in_str(in_str, target_letter):
    letter_count = functools.reduce(
        lambda count, letter: count + 1 if letter == target_letter else count, in_str, 0
    )
    return letter_count


def parse_input(input_set):
    """
    Function parses input like `1-3 a: abcde` into parts
    """
    parts = {}
    all_parts = input_set.split(" ")
    parts["password"] = all_parts[-1]
    parts["letter"] = all_parts[1][:-1]  # second part is letter with :
    letter_range = all_parts[0].split("-")
    parts["min_count"] = int(letter_range[0])
    parts["max_count"] = int(letter_range[1])
    return parts


def is_valid_password(input_set):
    input_parts = parse_input(input_set)
    letter_count = count_letter_in_str(input_parts["password"], input_parts["letter"])
    return input_parts["min_count"] <= letter_count <= input_parts["max_count"]


def count_valid_passwords(filename):
    num_valid_password = 0
    with open(filename) as f:
        for line in f:
            if is_valid_password(line):
                num_valid_password += 1
    return num_valid_password


print("Total: {}".format(count_valid_passwords(FILENAME)))
