FILENAME = "input.txt"


def is_one_letter_in_position(in_str, target_letter, first_pos, second_pos):
    is_letter_in_first_pos = in_str[first_pos] == target_letter
    is_letter_in_second_pos = in_str[second_pos] == target_letter
    # ^ is exclusive OR. Only one of the two sides must be True
    result = is_letter_in_first_pos ^ is_letter_in_second_pos
    return result


def parse_input(input_set):
    """
    Function parses input like `1-3 a: abcde` into parts
    """
    parts = {}
    all_parts = input_set.split(" ")
    parts["password"] = all_parts[-1]
    parts["letter"] = all_parts[1][:-1]  # second part is letter with :
    letter_range = all_parts[0].split("-")
    parts["first_pos"] = int(letter_range[0]) - 1
    parts["second_pos"] = int(letter_range[1]) - 1
    return parts


def is_valid_password(input_set):
    parts = parse_input(input_set)
    has_correct_positions = is_one_letter_in_position(
        in_str=parts["password"],
        target_letter=parts["letter"],
        first_pos=parts["first_pos"],
        second_pos=parts["second_pos"],
    )
    return has_correct_positions


def count_valid_passwords(filename):
    num_valid_password = 0
    with open(filename) as f:
        for line in f:
            if is_valid_password(line):
                num_valid_password += 1
    return num_valid_password


print("Total: {}".format(count_valid_passwords(FILENAME)))
