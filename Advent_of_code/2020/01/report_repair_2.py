#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import functools
import operator

FILENAME = "input.txt"


def can_sum_to_2020(nums):
    return sum(nums) == 2020


def first_true(iterable, default=False, pred=None):
    """Returns the first true value in the iterable.

    If no true value is found, returns *default*

    If *pred* is not None, returns the first item
    for which pred(item) is true.

    """
    # first_true([a,b,c], x) --> a or b or c or x
    # first_true([a,b], x, f) --> a if f(a) else b if f(b) else x
    return next(filter(pred, iterable), default)


def calculate_comb_sums(all_nums):
    three_num_comb = itertools.combinations(all_nums, 3)
    good_nums = first_true(three_num_comb, pred=can_sum_to_2020)
    return good_nums


def find_full_set(filename):
    all_nums = []
    with open(filename) as f:
        for line in f:
            all_nums.append(int(line))
    answer = functools.reduce(operator.mul, calculate_comb_sums(all_nums))
    return answer


print("Total: {}".format(find_full_set(FILENAME)))
