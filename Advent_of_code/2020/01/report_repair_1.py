#!/usr/bin/env python
# -*- coding: utf-8 -*-

FILENAME = "input.txt"


def find_full_set(filename):
    all_nums = []
    with open(filename) as f:
        for line in f:
            all_nums.append(int(line))
    required_nums = [2020 - num for num in all_nums]
    for idx, num in enumerate(required_nums):
        if num in all_nums:
            print(f"{num}, {all_nums[idx]}")
            good_number = num * all_nums[idx]
            break
    return good_number


print("Total: {}".format(find_full_set(FILENAME)))
