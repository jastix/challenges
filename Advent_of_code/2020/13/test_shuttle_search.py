from shuttle_search_1 import compute, compute_next_ts


def test_compute_next_ts():
    assert compute_next_ts(from_ts=3, bus_no=2) == 4


def test_sample():
    filename = "input_sample.txt"
    assert compute(filename=filename) == 295
