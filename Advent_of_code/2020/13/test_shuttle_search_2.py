from shuttle_search_2 import compute, find_solution


def test_sample():
    filename = "input_sample.txt"
    assert compute(filename=filename) == 1068781

def test_sample_1():
    vals = "17,x,13,19"
    assert find_solution(vals) == 3417


def test_sample_2():
    vals = "67,7,59,61"
    assert find_solution(vals) == 754018

def test_sample_3():
    vals = "1789,37,47,1889"
    assert find_solution(vals) == 1202161486
