import math

# Chinese Remainder Theorem is best approach for this problem.


def compute_next_ts(*, from_ts: int, bus_no: int) -> int:
    """Calculates next integer timestamp of bus arrival after from_ts
    """
    curr_bus_ts = math.ceil(from_ts / bus_no)
    return curr_bus_ts * bus_no

def read_data(*, filename=None) -> str:
    for line_no, row in enumerate(open(filename)):
        if line_no == 1:
            return row
    raise IOError('No row with number 1 was found')


def compute(*, filename=None) -> int:
    row_vals = read_data(filename=filename)
    result = find_solution(row_vals)
    return result


# code from Reddit: https://www.reddit.com/r/adventofcode/comments/kc4njx/2020_day_13_solutions/
def find_solution(row):
    bus_offset = compute_offsets(row)
    time, step = 0, 1
    for bus, offset in bus_offset.items():
        while (time + offset) % bus:
            time += step
        step *= bus
        print(f"bus: {bus}")
        print(f"step: {step}")
    return time

"""
Some explanation from Reddit:
Let's assume our input is 67,x,7,59,61 so buses = [(67, 0), (7, 2), (59, 3), (61, 4)]. Basic idea is to iterate over bigger and bigger step towards the answer.
We're going be iterating in steps so that current_time would always be correct answer if we just had all buses to the left of some current bus.
We're starting with bus [67, 0] so our first step is going to be 67 because multiples of 67 would always give us reminder of 0. Now fun begins,
we are first going to find multiple of 67 that satisfies next bus to the right (it could be any other bus, order doesn't matter) - (7, 2),
lowest multiple of 67 that has reminder (not exactly reminder but I forgot the word - the wait time) of 2 after dividing by 7 is 201 (17 - 201 % 17 == 2),
now we need to know how often that reminder is equal to 2 when we're increase using with step (67),
Now we know that if we're going to iterate from 201 in steps (67 * 7) we'll always satisfy first and second bus conditions (201, 670, 1139, ...).
Now basically we have new final_time = 201 and new delta = 67 * 7 = 469 and we'll do same step for next bus (59, 3) giving us final_time = 4422 and delta = 27671 and
for final iteration final_time is going to be 779210 which is the answer.
"""

# def find_solution(row):
#     """Attempt to find a solution by computing departure sequence
#     This does not finish in minutes.
#     """
#     bus_offset = compute_offsets(row)
#     max_bus_offset = max(bus_offset.items(), key=lambda x: x[0] + x[1])  # returns (bus_id, timestamp) tuple 
#     min_viable_offset = max_bus_offset[0] + max_bus_offset[1]
#     # min_viable_offset = 100000000000000
#     first_bus_id = min(bus_offset, key=bus_offset.get)
#     prev_bus_ts = 0
#     first_bus_ts = int(math.floor(min_viable_offset / first_bus_id)) * first_bus_id
#     print(first_bus_ts)
#     result = None
#     del bus_offset[first_bus_id]
#     sorted_other_buses = sorted(bus_offset.items(), key=lambda tup: tup[0] + tup[1], reverse=True)
#     while True:
#         seq_departures = []
#         first_bus_ts = first_bus_ts + first_bus_id
#         prev_bus_ts = first_bus_ts
#         result = first_bus_ts
#         for bus_id, offset in sorted_other_buses:
#             curr_bus_ts = prev_bus_ts + offset
#             if curr_bus_ts % bus_id != 0:
#                 seq_departures.append(False)
#                 break
#             else:
#                 seq_departures.append(True)
#         if not any(seq_departures):
#             continue
#         if all(seq_departures):
#             break
#     return result


def compute_offsets(row):
    """Function to parse data from input string into dict
    with proper data types.
    """
    row_vals = row.strip().split(',')
    bus_offset = {}
    for idx, val in enumerate(row_vals):
        if val.isdigit():
            bus_offset[int(val)] = idx
        else:
            continue
    return bus_offset


if __name__ == "__main__":
    print(compute(filename='input_sample_2.txt'))
