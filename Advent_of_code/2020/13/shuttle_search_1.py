import math


def compute_next_ts(*, from_ts: int, bus_no: int) -> int:
    """Calculates next integer timestamp of bus arrivala after from_ts
    """
    curr_bus_ts = math.ceil(from_ts / bus_no)
    return curr_bus_ts * bus_no


def compute(*, filename=None) -> int:
    for line_no, row in enumerate(open(filename)):
        if line_no == 0:
            timestamp = int(row)
        elif line_no == 1:
            row_vals = row.strip().split(',')
            # keeps only integer values from the line
            buses = [int(el) for el in row_vals if el.isdigit()]
        elif line_no > 1 and len(row.strip()) > 0:
            raise ValueError('There are more than 2 lines with values')
    next_tss = {bus: compute_next_ts(from_ts=timestamp, bus_no=bus) for bus in buses}
    earliest_bus = min(next_tss.items(), key=lambda x: x[1])  # returns (bus_id, timestamp) tuple
    wait_time = earliest_bus[1] - timestamp
    result = earliest_bus[0] * wait_time
    return result


if __name__ == "__main__":
    print(compute(filename='input.txt'))
