FILENAME = "input.txt"


def encountered_tree(input_line, pos):
    # if position is over the line then start from the beginning of the line
    if pos >= len(input_line):
        pos = pos - len(input_line)
    return (input_line[pos] == '#', pos)


def count_trees(filename):
    num_trees = 0
    pos = 0  # initial position
    with open(filename) as f:
        next(f)  # skip first line
        for line in f:
            pos += 3
            is_tree, pos = encountered_tree(line.strip(), pos)
            if is_tree:
                num_trees += 1
    return num_trees


print("Total: {}".format(count_trees(FILENAME)))
