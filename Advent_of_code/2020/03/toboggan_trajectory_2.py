import functools
import operator

FILENAME = "input.txt"


def encountered_tree(input_line, pos):
    # if position is over the line then start from the beginning of the line
    if pos >= len(input_line):
        pos = pos - len(input_line)
    return (input_line[pos] == "#", pos)


def count_trees_for_slope(filename, movement):
    """
    Count encountered trees for each particular movement setting
    """
    pos = 0
    num_trees = 0
    with open(filename) as f:
        num_trees = 0
        line_to_check = movement["down"]
        for line_num, line in enumerate(f):
            if line_to_check == line_num:
                pos += movement["right"]
                line_to_check += int(movement["down"])
                is_tree, pos = encountered_tree(line.strip(), pos)
                if is_tree:
                    num_trees += 1
    return num_trees


def count_trees(filename, movements):
    all_slopes_trees = []
    for movement in movements:
        num_trees = count_trees_for_slope(filename, movement)
        all_slopes_trees.append(num_trees)
    mult_trees = functools.reduce(operator.mul, all_slopes_trees)
    return mult_trees


MOVEMENTS = [
    {"right": 1, "down": 1},
    {"right": 3, "down": 1},
    {"right": 5, "down": 1},
    {"right": 7, "down": 1},
    {"right": 1, "down": 2},
]

print("Total: {}".format(count_trees(FILENAME, MOVEMENTS)))
