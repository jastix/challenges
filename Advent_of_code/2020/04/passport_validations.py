import re


def validate_year(in_str, start_year, end_year):
    in_str_clean = in_str.strip()
    is_valid_length = len(in_str_clean) == 4
    if not is_valid_length:
        return False
    is_digit = in_str_clean.isdigit()
    if not is_digit:
        return False
    within_years = int(start_year) <= int(in_str_clean) <= int(end_year)
    return within_years


def validate_byr(in_str):
    return validate_year(in_str, start_year=1920, end_year=2002)


def validate_iyr(in_str):
    return validate_year(in_str, start_year=2010, end_year=2020)


def validate_eyr(in_str):
    return validate_year(in_str, start_year=2020, end_year=2030)


def validate_hgt(in_str):
    pattern = re.compile(r"(\d+)")
    parts = re.split(pattern, in_str.strip())  # split by numbers
    parse_height = [p.strip() for p in parts if p.strip() != ""]
    if len(parse_height) < 2:
        return False
    height = parse_height[0]
    unit = parse_height[1]
    if unit not in ("cm", "in"):
        return False
    is_digit = height.isdigit()
    if not is_digit:
        return False
    if unit == "cm":
        within_height = 150 <= int(height) <= 193
    else:
        within_height = 59 <= int(height) <= 76
    return within_height


def validate_hcl(in_str):
    valid_color_length = 6
    pattern = re.compile(r"([0-9a-f]){6}")
    in_str_clean = in_str.strip()
    has_hash = in_str_clean[0] == "#"
    if not has_hash:
        return False
    color = in_str_clean[1:]
    if not (len(color) == valid_color_length):
        return False
    matched_color = re.search(pattern, in_str_clean)
    if not matched_color:
        return False
    return True


def validate_ecl(in_str):
    valid_colors = ("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
    in_str_clean = in_str.strip()
    if not (in_str_clean in valid_colors):
        return False
    return True


def validate_pid(in_str):
    in_str_clean = in_str.strip()
    valid_pid_length = 9
    pattern = re.compile(rf"^\d{{{valid_pid_length}}}$")
    matched_number = re.search(pattern, in_str_clean)
    if not matched_number:
        return False
    return True


def validate_cid(in_str):
    return True
