from passport_processing_1 import count_valid_passports, REQUIRED_FIELS


def test_count_valid_passports():
    input_file = 'input_test.txt'
    assert count_valid_passports(input_file, REQUIRED_FIELS) == 2
