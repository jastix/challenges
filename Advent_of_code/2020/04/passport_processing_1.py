FILENAME = "input.txt"


def parse_line(input_line):
    # extracts keys from string like 'eyr:1972 cid:100'
    kvs = input_line.split(' ')
    ks = set([kv.split(':')[0] for kv in kvs])
    return ks


def count_valid_passports(filename, required_fields):
    num_valid_passports = 0
    with open(filename) as f:
        seen_fields = set()
        for line in f:
            if len(line.strip()) > 0:
                line_fields = parse_line(line.strip())
                seen_fields = seen_fields.union(line_fields)
            else:
                is_valid_passport = required_fields.issubset(seen_fields)
                if is_valid_passport:
                    num_valid_passports += 1
                seen_fields = set()
        # last line processing
        if seen_fields:
            is_valid_passport = required_fields.issubset(seen_fields)
            if is_valid_passport:
                num_valid_passports += 1
            seen_fields = set()

    return num_valid_passports


REQUIRED_FIELS = set(['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'])

print("Total: {}".format(count_valid_passports(FILENAME, REQUIRED_FIELS)))
