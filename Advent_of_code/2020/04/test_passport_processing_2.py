from passport_processing_2 import (
    count_valid_passports,
    REQUIRED_FIELS,
)
from passport_validations import (
    validate_byr,
    validate_iyr,
    validate_eyr,
    validate_hgt,
    validate_hcl,
    validate_ecl,
    validate_pid
)


def test_validate_byr():
    assert validate_byr("2002") is True
    assert validate_byr("2003") is False


def test_validate_iyr():
    assert validate_iyr("2011") is True
    assert validate_iyr("2021") is False


def test_validate_eyr():
    assert validate_eyr("2020") is True
    assert validate_eyr("2031") is False


def test_validate_hgt():
    assert validate_hgt("60in") is True
    assert validate_hgt("190")  is False


def test_validate_hcl():
    assert validate_hcl('#123abc') is True
    assert validate_hcl('123abc')  is False
    assert validate_hcl('#123abz') is False


def test_validate_ecl():
    assert validate_ecl('brn') is True
    assert validate_ecl('wat') is False


def test_validate_pid():
    assert validate_pid('  000000001') is True
    assert validate_pid('0123456789')  is False


def test_count_valid_passports():
    input_file = 'input_test_2_valid.txt'
    assert count_valid_passports(input_file, REQUIRED_FIELS) == 4


def test_count_invalid_passports():
    input_file = 'input_test_2_invalid.txt'
    assert count_valid_passports(input_file, REQUIRED_FIELS) == 0
