from passport_validations import (
    validate_byr,
    validate_iyr,
    validate_eyr,
    validate_hgt,
    validate_hcl,
    validate_ecl,
    validate_pid
)


FILENAME = "input.txt"  # 127 valid passports


def parse_line(input_line):
    # extracts keys and values from string like 'eyr:1972 cid:100'
    kvs = input_line.split(" ")
    ks = {kv.split(":")[0]: kv.split(":")[1] for kv in kvs}
    return ks


def count_valid_passports(filename, required_fields):
    num_valid_passports = 0
    with open(filename) as f:
        seen_fields = dict()
        for line in f:
            if len(line.strip()) > 0:
                line_fields = parse_line(line.strip())
                seen_fields = (seen_fields | line_fields)
            else:
                required_fields_set = set(required_fields.keys())
                seen_fields.pop('cid', None)  # field cid is not being checked
                seen_fields_set = set(seen_fields.keys())
                has_required_fields = required_fields_set.issubset(seen_fields_set)
                if has_required_fields:
                    fields_validity = []
                    for k in seen_fields:
                        is_valid_field = required_fields[k](seen_fields[k])
                        fields_validity.append(is_valid_field)
                    all_fields_valid = all(fields_validity)
                    if all_fields_valid:
                        num_valid_passports += 1
                seen_fields = dict()
        # last line processing
        if seen_fields:
            required_fields_set = set(required_fields.keys())
            seen_fields.pop('cid', None)
            seen_fields_set = set(seen_fields.keys())
            has_required_fields = required_fields_set.issubset(seen_fields_set)
            if has_required_fields:
                fields_validity = []
                for k in seen_fields:
                    is_valid_field = required_fields[k](seen_fields[k])
                    fields_validity.append(is_valid_field)
                all_fields_valid = all(fields_validity)
                if all_fields_valid:
                    num_valid_passports += 1
            seen_fields = dict()

    return num_valid_passports


REQUIRED_FIELS = {
    "byr": validate_byr,
    "iyr": validate_iyr,
    "eyr": validate_eyr,
    "hgt": validate_hgt,
    "hcl": validate_hcl,
    "ecl": validate_ecl,
    "pid": validate_pid,
}


if __name__ == '__main__':
    print("Total: {}".format(count_valid_passports(FILENAME, REQUIRED_FIELS)))
