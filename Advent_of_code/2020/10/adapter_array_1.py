def read_file(name):
    for row in open(name, "r"):
        yield row


# function to compute differences between pairs of elements
# since python 3.10 there is a built-in function "itertools.pairwise"
def pairwise_diff(items):
    result = [items[i] - items[i - 1] for i in range(1, len(items))]
    return result


def compute(input_file='input.txt'):
    outlet_j = 0  # initial
    inp = sorted((int(val.strip()) for val in read_file(input_file)))
    device_j = inp[len(inp) - 1] + 3  # final value in for all jolts
    all_jolts = [outlet_j, *inp, device_j]
    diffs = pairwise_diff(all_jolts)
    num_1s = sum(1 for it in diffs if it == 1)
    num_3s = sum(1 for it in diffs if it == 3)
    result = num_1s * num_3s
    return result


if __name__ == "__main__":
    print(compute())
