from adapter_array_1 import compute

def test_sample_1():
    filename = 'input_sample.txt'
    assert compute(input_file=filename) == 35

def test_sample_2():
    filename = 'input_sample_2.txt'
    assert compute(input_file=filename) == 220
