from encoding_error import check_sums, compute


def test_check_sums_valid_26():
    inp = range(1, 26, 1)
    target = 26
    assert check_sums(inp, target) == True


def test_check_sums_invalid_100():
    inp = range(1, 26, 1)
    target = 100
    assert check_sums(inp, target) == False

def test_compute_result():
    assert compute(pream_size=25) == 85848519