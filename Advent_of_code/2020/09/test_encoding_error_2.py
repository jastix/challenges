from encoding_error_2 import run


def test_compute_result_sample():
    preamble_size = 5
    input_file = 'input_sample.txt'
    assert run(pream_size=preamble_size, input_file=input_file) == 62

def test_compute_result_final():
    preamble_size = 25
    input_file = 'input.txt'
    assert run(pream_size=preamble_size, input_file=input_file) == 13414198