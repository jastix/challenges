from encoding_error import read_file
from encoding_error import compute as compute_1


# function to recalculate all possible contiguous elements sums
def calc_sums(items, bad_num):
    result = None
    min_check_length = 2
    max_check_length = len(items)
    for size in range(min_check_length, max_check_length, 1):
        curr_items = items[:size]
        curr_sum = sum(curr_items)
        if curr_sum > bad_num:
            break
        if bad_num == curr_sum:
            result = min(curr_items) + max(curr_items)
    return result


def compute(bad_num, input_file):
    curr_items = []
    for line in read_file(input_file):
        num = int(line.strip())
        if len(curr_items) >= 2:
            all_sum = sum(curr_items)
            result = calc_sums(curr_items, bad_num)
            if result:
                return result
            elif all_sum > bad_num:
                curr_items.pop(0)
        curr_items.append(num)


def run(pream_size, input_file):
    bad_num = compute_1(pream_size=pream_size, input_file=input_file)
    result = compute(bad_num, input_file)
    return result


if __name__ == "__main__":
    input_file = "input.txt"
    print(run(pream_size=25, input_file=input_file))
