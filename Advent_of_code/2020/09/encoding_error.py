"""
keep only last n (preamble size) items
make sums of 25 previous numbers (zip)
is current item in sums?
  break if not
"""

import itertools

# file reading code extracted for reusability
def read_file(name):
    for row in open(name, "r"):
        yield row


def compute(pream_size, input_file="input.txt"):
    pr_items = []

    for line in read_file(input_file):
        num = int(line.strip())
        if len(pr_items) == pream_size:
            if not check_sums(pr_items, num):
                return num
            pr_items.pop(0)
        pr_items.append(num)


def check_sums(all_nums, target, comb_size=2):
    combs = itertools.combinations(all_nums, comb_size)
    all_sums = (sum(c) for c in combs)  # generator to avoid computing the whole list
    result = target in all_sums
    return result


if __name__ == "__main__":
    print(compute(pream_size=25))
