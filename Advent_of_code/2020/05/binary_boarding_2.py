def calculate_position(in_str, val_range=None, upper_code=None, lower_code=None):
    rows_range = range(val_range)
    codes = list(in_str)
    for code in codes:
        split_point = (
            len(rows_range) // 2
        )  # operator // is integer division in Python 3
        if code == upper_code:
            rows_range = rows_range[:split_point]
        elif code == lower_code:
            rows_range = rows_range[split_point:]
        else:
            raise ValueError(f"Unknown code: {code}")
    return rows_range[0]


def calculate_seat_id(in_str):
    row = calculate_position(in_str[:7], val_range=128, upper_code="F", lower_code="B")
    column = calculate_position(in_str[7:], val_range=8, upper_code="L", lower_code="R")
    return row * 8 + column


def find_highest_seat_id(filename):
    highest_seat_id = 0
    lowest_seat_id = None
    seen_seat_ids = set()
    with open(filename) as f:
        for line in f:
            seat_id = calculate_seat_id(line.strip())
            if seat_id > highest_seat_id:
                highest_seat_id = seat_id
            if lowest_seat_id is None:
                lowest_seat_id = seat_id
            if seat_id < lowest_seat_id:
                lowest_seat_id = seat_id
            seen_seat_ids.add(seat_id)
    all_seats = range(lowest_seat_id, highest_seat_id + 1)
    missing_seats = set(all_seats) - seen_seat_ids
    return missing_seats


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Missing seat: {}".format(find_highest_seat_id(FILENAME)))  # 587
