from binary_boarding_1 import (
    calculate_position,
    calculate_seat_id,
    find_highest_seat_id,
)


def test_calculate_row():
    assert (
        calculate_position("FBFBBFF", val_range=128, upper_code="F", lower_code="B")
        == 44
    )
    assert (
        calculate_position("BFFFBBF", val_range=128, upper_code="F", lower_code="B")
        == 70
    )
    assert (
        calculate_position("FFFBBBF", val_range=128, upper_code="F", lower_code="B")
        == 14
    )
    assert (
        calculate_position("BBFFBBF", val_range=128, upper_code="F", lower_code="B")
        == 102
    )


def test_calculate_column():
    assert calculate_position("RLR", val_range=8, upper_code="L", lower_code="R") == 5
    assert calculate_position("RRR", val_range=8, upper_code="L", lower_code="R") == 7
    assert calculate_position("RLL", val_range=8, upper_code="L", lower_code="R") == 4


def test_calculate_seat_id():
    assert calculate_seat_id("FBFBBFFRLR") == 357
    assert calculate_seat_id("FFFBBBFRRR") == 119
    assert calculate_seat_id("BBFFBBFRLL") == 820


def test_find_highest_seat_id():
    assert find_highest_seat_id("input_test_1.txt") == 820
