import re


def normalize_bag_name(bag_name):
    return bag_name.split("bag")[0].strip()


def parse_inside_bags(inside):
    splitted = re.split(r"(\d+)", inside)
    bags = []
    for part in splitted:
        if "bag" in part:
            bag_name = normalize_bag_name(part)
            if bag_name == "no other":
                pass
            else:
                bags.append(bag_name)
    return bags


def parse_rule(rule):
    parts = rule.split("contain")
    rule = dict()
    main_bag = normalize_bag_name(parts[0])
    inside = parse_inside_bags(parts[1])
    rule[main_bag] = inside
    return rule


def build_rules(filename):
    rules = dict()
    with open(filename) as f:
        for line in f:
            parsed_rule = parse_rule(line)
            rules |= parsed_rule
    return rules


def find_shiny_bag(inside, rules):
    contains_shiny_bag = False
    while True:
        if inside == []:
            break
        if inside[0] == "shiny gold":
            contains_shiny_bag = True
            break
        inside = rules[inside[0]] + inside[1:]
    return contains_shiny_bag


def count_bags_with_shiny(filename):
    num_contained_shiny_bag = 0
    rules = build_rules(filename)
    for bag, inside in rules.items():
        if find_shiny_bag(inside, rules) is True:
            num_contained_shiny_bag += 1
    return num_contained_shiny_bag


if __name__ == "__main__":
    FILENAME = "input.txt"
    print("Total: {}".format(count_bags_with_shiny(FILENAME)))  # 101
