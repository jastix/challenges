from handy_haversacks_1 import count_bags_with_shiny, parse_rule, build_rules


def test_parse_rule():
    rule = "light red bags contain 1 bright white bag, 2 muted yellow bags."
    assert parse_rule(rule) == {"light red": ["bright white", "muted yellow"]}


def test_rule_no_other_bags():
    rule = "faded blue bags contain no other bags."
    assert parse_rule(rule) == {"faded blue": []}


def test_build_rules():
    input_file = "input_test_1.txt"
    assert build_rules(input_file) != {}


def test_build_rules_terminal():
    input_file = "input_test_1.txt"
    rules = build_rules(input_file)
    num_terminal_rules = len([1 for k, v in rules.items() if v == []])
    assert num_terminal_rules > 0


def test_count_bags_with_shiny_bags():
    input_file = "input_test_1.txt"
    assert count_bags_with_shiny(input_file) == 4
