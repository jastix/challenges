#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

FILENAME = "input.txt"


def calculate_individual_requirement(mass):
    return math.floor(mass / 3) - 2


def calculate_fuel_requirements(filename):
    total = 0
    with open(filename) as f:
        for _, line in enumerate(f):
            total += calculate_individual_requirement(int(line))
    return total


print("Total: {}".format(calculate_fuel_requirements(FILENAME)))
