#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rocket_equation_2 import calculate_individual_requirement, calculate_fuel_for_fuel_requirement


def test_calculate_requirement_1():
    mass_fuel = calculate_individual_requirement(14)
    fuel_for_fuel = calculate_fuel_for_fuel_requirement(mass_fuel)
    assert (mass_fuel + fuel_for_fuel) == 2


def test_calculate_requirement_2():
    mass_fuel = calculate_individual_requirement(1969)
    fuel_for_fuel = calculate_fuel_for_fuel_requirement(mass_fuel)
    total = (mass_fuel + fuel_for_fuel)
    assert total == 966


def test_calculate_requirement_3():
    mass_fuel = calculate_individual_requirement(100756)
    fuel_for_fuel = calculate_fuel_for_fuel_requirement(mass_fuel)
    total = (mass_fuel + fuel_for_fuel)
    assert total == 50346
