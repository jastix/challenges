#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rocket_equation import calculate_individual_requirement


def test_calculate_requirement_1():
    assert calculate_individual_requirement(12) == 2


def test_calculate_requirement_2():
    assert calculate_individual_requirement(1969) == 654

