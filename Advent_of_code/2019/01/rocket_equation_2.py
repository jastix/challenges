#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

FILENAME = "input.txt"


def calculate_individual_requirement(mass):
    return math.floor(mass / 3) - 2


def calculate_fuel_for_fuel_requirement(fuel_amount):
    fuel_for_fuel_req = 0
    while True:
        req = calculate_individual_requirement(fuel_amount)
        if req <= 0:
            break
        fuel_for_fuel_req += req
        fuel_amount = req
    return fuel_for_fuel_req


def calculate_fuel_requirements(filename):
    total = 0
    with open(filename) as f:
        for _, line in enumerate(f):
            mass_fuel = calculate_individual_requirement(int(line))
            fuel_for_fuel = calculate_fuel_for_fuel_requirement(mass_fuel)
            total += mass_fuel + fuel_for_fuel
    return total


print("Total: {}".format(calculate_fuel_requirements(FILENAME)))
