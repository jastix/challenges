#!/usr/bin/env python
# -*- coding: utf-8 -*-

from program_alarm import restore_program

def test_demo_program():
    input = "1,9,10,3,2,3,11,0,99,30,40,50"
    assert restore_program(input) == [3500,9,10,70,2,3,11,0,99,30,40,50]

def test_demo_program_1():
    input = "1,0,0,0,99"
    assert restore_program(input) == [2,0,0,0,99]

def test_demo_program_2():
    input = "2,3,0,3,99"
    assert restore_program(input) == [2,3,0,6,99]

def test_demo_program_3():
    input = "2,4,4,5,99,0"
    assert restore_program(input) == [2,4,4,5,99,9801]

def test_demo_program_4():
    input = "1,1,1,4,99,5,6,0,99"
    assert restore_program(input) == [30,1,1,4,2,5,6,0,99]
