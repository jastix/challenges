#!/usr/bin/env python
# -*- coding: utf-8 -*-
import operator


def prepare_input(program):
    return list(map(int, program.split(',')))


def process_opcode(program, position, op):
    return op(program[program[position+1]], program[program[position+2]])


def process_opcodes(program, position=0):
    while True:
        if program[position] == 1:
            result = process_opcode(program, position, operator.add)
            program[program[position+3]] = result
            position += 4
        elif program[position] == 2:
            result = process_opcode(program, position, operator.mul)
            program[program[position+3]] = result
            position += 4
        elif program[position] == 99:
            return program
        else:
            raise ValueError

    return program

def restore_program(program):
    if isinstance(program, str):
        program = prepare_input(program)
    return process_opcodes(program)

def patch_input(program):
    program[1] = 12
    program[2] = 2
    return program

def main():
    filename = "input.txt"
    with open(filename) as f:
        for _, line in enumerate(f):
            prepared_input = prepare_input(line)
            patched_input = patch_input(prepared_input)
            result = restore_program(patched_input)
            print(result)
            return result

if __name__ == "__main__":
    main()
