defmodule Trampoline do
  @moduledoc """
  Documentation for Trampoline.
  """

  def input(filename) do
    {:ok, file} = File.open(filename, [:read])
    IO.read(file, :all)
    |> count
  end

  def count(str) do
    str
    |> String.splitter("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> create_mapping(0, %{})
    |> do_jump()
  end

  defp create_mapping([], _counter, acc), do: acc
  defp create_mapping([h|t], counter, acc) do
    create_mapping(t, counter + 1, Map.put(acc, counter, h))
  end

  defp do_jump(mapping) do
    jump(mapping, Enum.count(mapping), 0, 0)
  end

  defp jump(_mapping, map_size, current_pos, counter)
    when current_pos >= map_size do
      counter
  end
  defp jump(mapping, map_size, current_pos, counter) do
    current_value = Map.get(mapping, current_pos)
    next_pos = current_pos + current_value  
    updated_value = current_value + 1
    jump(
      %{ mapping | current_pos => updated_value },
      map_size,
      next_pos,
      counter + 1
    )
  end

  # part 2
  def input2(filename) do
    {:ok, file} = File.open(filename, [:read])
    IO.read(file, :all)
    |> count2
  end

  def count2(str) do
    str
    |> String.splitter("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> create_mapping(0, %{})
    |> do_jump2()
  end

  defp do_jump2(mapping) do
    jump2(mapping, Enum.count(mapping), 0, 0)
  end

  defp jump2(_mapping, map_size, current_pos, counter)
  when current_pos >= map_size do
    counter
  end

  defp jump2(mapping, map_size, current_pos, counter) do
    current_value = Map.get(mapping, current_pos)
    next_pos = current_pos + current_value  
    updated_value = update_value(current_value)
    jump2(
      %{ mapping | current_pos => updated_value },
      map_size,
      next_pos,
      counter + 1
    )
  end

  defp update_value(current) do
    case current >= 3 do
      true  -> current - 1
      false -> current + 1
    end
  end
end
