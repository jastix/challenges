defmodule TrampolineTest do
  use ExUnit.Case

  test "counts jumps to escape" do
    assert Trampoline.count("0\n3\n0\n1\n-3") == 5
  end

  test "counts jumps to escape 2" do
    assert Trampoline.count2("0\n3\n0\n1\n-3") == 10
  end
end
