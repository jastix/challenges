defmodule Passphrase do
  @moduledoc """
  Passphrase should not contain repeated parts and anagrams.
  """

  def count_valids1(input) do
    {:ok, file} = File.open(input, [:read])
    IO.read(file, :all)
    |> verify1()
  end

  def verify1(input) do
    input
    |> String.split("\n", trim: true)
    |> Stream.map(&split_to_elements/1)
    |> Stream.map(&has_duplicates?/1)
    |> Enum.count(&(&1))
  end

  def count_valids2(input) do
    {:ok, file} = File.open(input, [:read])
    IO.read(file, :all)
    |> verify2()
  end

  def verify2(input) do
    input
    |> String.split("\n", trim: true)
    |> Stream.map(&split_to_elements/1)
    |> Stream.map(&has_anagrams?/1)
    |> Enum.count(&(&1))
  end

  def split_to_elements(input) do
    input
    |> String.trim()
    |> String.split(" ", trim: true)
  end

  def has_duplicates?(phrase) do
    phrase |> has_unique_elements?()
  end

  def has_anagrams?(phrase) do
    phrase
    |> Stream.map(fn(el) -> el |> String.codepoints() |> Enum.sort() end)
    |> has_unique_elements?()
  end

  defp has_unique_elements?(elems) do
    elems |> MapSet.new() |> MapSet.size() >=
    elems |> Enum.count()
  end
end
