defmodule PassphraseTest do
  use ExUnit.Case

  test "counts valid phrases from file" do
    assert Passphrase.count_valids1("test/test_input") == 2
  end

  test "counts valid phrases from file with anagrams" do
    assert Passphrase.count_valids2("test/test_input_anagrams") == 3
  end
end
