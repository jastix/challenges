phrases = %{
"long" => "bxebny akknwxw jeyxqvj syl cedps akknwxw akknwxw zpvnf kuoon pnkejn wqjgc"
          |> String.duplicate(10)
}

Benchee.run %{
  "count_valids1" => fn(list) -> Passphrase.verify1(list) end,
  "count_valids2" => fn(list) -> Passphrase.verify2(list) end
}, time: 10, inputs: phrases
