defmodule CaptchaTest do
  use ExUnit.Case
  doctest Captcha

  test "sums 1122" do
    assert Captcha.solve("1122") == 3
  end

  test "sums 1111" do
    assert Captcha.solve("1111") == 4
  end

  test "sums 1234" do
    assert Captcha.solve("1234") == 0
  end

  test "sums 91212129" do
    assert Captcha.solve("91212129") == 9
  end

  test "part 2 sums 1212" do
    assert Captcha.solve2("1212") == 6
  end

  test "part 2 sums 1221" do
    assert Captcha.solve2("1221") == 0
  end

  test "part 2 sums 123425" do
    assert Captcha.solve2("123425") == 4
  end

  test "part 2 sums 123123" do
    assert Captcha.solve2("123123") == 12
  end

  test "part sums 12131415" do
    assert Captcha.solve2("12131415") == 4
  end
end
