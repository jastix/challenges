nums = %{
"Huge" => Enum.random(123456789123456789123456789..912345678912345678912345678)
          |> Integer.to_string
          |> String.duplicate(10),
"Small" => Enum.random(123456789123456789123456789..912345678912345678912345678) |> Integer.to_string
}

Benchee.run %{
  "Captcha day 1_1" =>
    fn(list) -> Captcha.solve(list) end,
  "Captcha day 1_2" =>
    fn(list) -> Captcha.solve2(list) end,
}, time: 10, inputs: nums
