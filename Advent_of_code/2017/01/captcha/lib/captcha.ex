defmodule Captcha do
  @moduledoc """
  Solves a captcha that expects a sum of digits that have the same next digit in
  a sequence.
  Or a the same digit halfway through the sequence.
  """

  @doc """
  Solves a captcha.

  ## Examples

      iex> Captcha.solve("1111")
      4

  """
  def solve(num) do
    nums = num |> String.graphemes
    sum(nums ++ [hd(nums)], 0)
  end

  defp sum([h , h | t], acc) do
    sum([h | t], acc + String.to_integer(h))
  end
  defp sum([_h, n | t], acc), do: sum([n | t], acc)
  defp sum([_h | []], acc),   do: acc


  def solve2(num) do
    nums = num |> String.graphemes
    offset = Enum.count(nums) |> div(2)
    nums_till_offset = Enum.take(nums, offset)
    nums_reordered = nums |> Enum.drop(offset) |> Enum.concat(nums_till_offset)
    sum2(nums, nums_reordered, 0)
  end

  defp sum2([h | t], [h | ts], acc) do
    sum2(t, ts, acc + String.to_integer(h))
  end
  defp sum2([_h | t], [_hs | ts], acc), do: sum2(t, ts, acc)
  defp sum2([], _next, acc),            do: acc
end
