defmodule ChecksumTest do
  use ExUnit.Case

  test "computes test checksum" do
    assert Checksum.compute("test/test_input_1") == 18
  end

  test "computes test checksum part 2" do
    assert Checksum.compute2("test/test_input_2") == 9
  end
end
