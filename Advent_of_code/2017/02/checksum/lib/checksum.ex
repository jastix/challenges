defmodule Checksum do
  @moduledoc """
  Computes a checksum, which is defined as a sum of differences between min and
  max values for each row.
  """

  def compute(input) do
    {:ok, file} = File.open(input, [:read])
    IO.read(file, :all)
    |> String.trim()
    |> String.split("\n", trim: true)
    |> Stream.map(&String.split(&1, "\t", trim: true))
    |> Stream.map(fn(x) -> Enum.map(x, fn(y) -> String.to_integer(y) end) end)
    |> Stream.map(&Enum.min_max(&1))
    |> Stream.map(fn({min, max}) -> max - min end)
    |> Enum.sum()
  end

  @doc """
  Computes a checksum, which is defined as a sum of result of division
  of numbers that divide each other without a remainder
  """

  def compute2(input) do
    {:ok, file} = File.open(input, [:read])
    IO.read(file, :all)
    |> String.trim()
    |> String.split("\n", trim: true)
    |> Stream.map(&String.split(&1, "\t", trim: true))
    |> Stream.map(fn(x) -> Enum.map(x, fn(y) -> String.to_integer(y) end) end)
    |> Stream.flat_map(&evenly_divide_nums(&1))
    |> Enum.sum()
  end

  defp evenly_divide_nums(nums) do
    for j <- nums,
        k <- nums,
        j > k,
        rem(j, k) == 0,
        do: div(j, k)
  end
end
