"""
https://adventofcode.com/2023/day/1
"""

import sys


def compute(*, filename) -> int:
    constraints = {"red": 12, "green": 13, "blue": 14}
    possible_game_ids = []
    for line in read_file(filename):
        game_name, all_games = line.strip().split(":")
        is_valid_game: bool = check_games(all_games, constraints)
        if is_valid_game:
            game_id = int(game_name.split(" ")[1])
            possible_game_ids.append(game_id)
    result = sum(possible_game_ids)
    return result


def check_games(all_games: str, constraints: dict[str, int]) -> bool:
    result: bool = True
    games = all_games.split(";")
    for game in games:
        cubes = game.split(",")
        for cube in cubes:
            val, color = cube.strip().split(" ")
            if constraints[color] < int(val):
                result = False
                break
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 2278
