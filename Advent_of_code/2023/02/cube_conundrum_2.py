"""
https://adventofcode.com/2023/day/1
"""

import sys
import operator
import functools
from typing import Dict, List


def compute(*, filename: str) -> int:
    cube_powers: List[int] = []
    for line in read_file(filename):
        _, all_games = line.strip().split(":")
        min_cube_config = check_games(all_games)
        cube_power: int = functools.reduce(operator.mul, min_cube_config.values())
        cube_powers.append(cube_power)
    result = sum(cube_powers)
    return result


def check_games(all_games: str) -> Dict[str, int]:
    min_cube_config = {"blue": 0, "green": 0, "red": 0}
    games = all_games.split(";")
    for game in games:
        cubes = game.split(",")
        for cube in cubes:
            val, color = cube.strip().split(" ")
            if min_cube_config[color] < int(val):
                min_cube_config[color] = int(val)
    return min_cube_config


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 67953
