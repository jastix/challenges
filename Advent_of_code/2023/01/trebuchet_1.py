"""
https://adventofcode.com/2023/day/1
"""

import sys


def compute(*, filename) -> int:
    all_values = []
    for line in read_file(filename):
        first_digit = find_digit(line)
        last_digit = find_digit(line, mode="last")
        val = first_digit + last_digit
        all_values.append(int(val))
    result = sum(all_values)
    return result


def find_digit(inp: str, mode="first") -> str:
    result = ''
    if mode == "last":
        inp = reversed(inp)
    for char in inp:
        if char.isnumeric():
            result = char
            break
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 55090
