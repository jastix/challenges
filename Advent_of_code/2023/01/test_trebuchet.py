from trebuchet_1 import compute
from trebuchet_2 import compute as compute_2


def test_sample_1():
    filename = "input_sample.txt"
    assert compute(filename=filename) == 142


def test_sample_2():
    filename = "input_sample_2.txt"
    assert compute_2(filename=filename) == 281
