"""
https://adventofcode.com/2023/day/1
"""

import sys
from operator import itemgetter


def compute(*, filename) -> int:
    all_values = []

    for line in read_file(filename):
        converted_line = convert_digits(line.strip())
        first_digit = find_digit(converted_line)
        # in a line with only 1 digit the same digit counts twice
        last_digit = find_digit(converted_line, mode="last")
        val = first_digit + last_digit
        all_values.append(int(val))
    result = sum(all_values)
    return result


def findall(p, s):
    """Yields all the positions of
    the pattern p in the string s."""
    i = s.find(p)
    while i != -1:
        yield i
        i = s.find(p, i + 1)


def convert_digits(inp: str) -> str:
    conversion = {
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9",
    }
    matches = list()
    for k, v in conversion.items():
        if k in inp:
            founds = findall(k, inp)
            if founds:
                for idx in founds:
                    matches.append((idx, k, v))
    matches.sort(key=itemgetter(0))
    for m in matches:
        idx = m[0]
        pos = inp[idx:].startswith(m[1])
        if pos:
            # overlapping digit names are counted as separate digits
            inp = inp[:idx] + m[2] + inp[idx + 1:]
    return inp


def find_digit(inp: str, mode="first") -> str:
    result = ''
    if mode == "last":
        inp = reversed(inp)
    for char in inp:
        if char.isnumeric():
            result = char
            break
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 54845
