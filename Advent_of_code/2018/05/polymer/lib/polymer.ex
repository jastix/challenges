defmodule Polymer do
  @moduledoc """
  Removes units with opposite polarization
  (sequence of letters with opposite capitalization).
  """

  def start1(filename) do
    filename
    |> prepare_input
    |> do_reaction
  end

  def start2(filename) do
    input = filename |> prepare_input

    units = input |> Enum.reduce(%{}, fn ch, acc ->
      if ch in ?a..?z do
        Map.put(acc, ch, 0)
      else acc
      end
    end)

    units
    |> Map.keys()
    |> Task.async_stream(&react(&1, input), [timeout: 1000000, max_concurrency: 4])
    |> Enum.reduce(%{}, fn {:ok, {v, c}}, acc -> Map.put(acc, v, c) end)
    |> Map.values
    |> Enum.min
  end

  def react(v, input) do
    v
    |> stream_filter(input) 
    |> do_reaction
    |> create_tuple(v)
  end

  def do_reaction(value) do
    value
    |> Stream.chunk_every(1000)
    |> Task.async_stream(&Polymer.compute/1)
    |> Enum.reduce([], fn {:ok, part}, acc -> acc ++ [part] end)
    |> List.to_string()
    |> String.to_charlist()
    |> compute()
    |> Enum.count()
  end

  def create_tuple(count, v) do
    {v, count}
  end

  def stream_filter(value, st) do
    Stream.filter(st, fn x -> x not in [value, value - 32] end)
  end

  def prepare_input(filename) do
    filename
    |> File.stream!()
    |> Stream.flat_map(&String.split(&1, "\n", trim: true))
    |> Stream.flat_map(&String.to_charlist/1)
  end

  def compute([h | t]), do: compute(t, [h])
  def compute([h | t], []), do: compute(t, [h])

  def compute([h | t], [hs | ts]) do
    case abs(h - hs) == 32 do
      true -> compute(t, ts)
      false -> compute(t, [h | [hs | ts]])
    end
  end

  def compute([], seen), do: Enum.reverse(seen)
end
