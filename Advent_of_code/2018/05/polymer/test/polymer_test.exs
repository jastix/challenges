defmodule PolymerTest do
  use ExUnit.Case

  test "computes polymer" do
    assert Polymer.start1("test/test.txt") == 10
  end

  test "computes final polymer" do
    assert Polymer.start1("data.txt") == 10250
  end

  # test "computes shortest polymer" do
  #   assert Polymer.start2("test/test2.txt") == 4
  # end

  test "computes final shortest polymer" do
    assert Polymer.start2("data.txt") == 6188
  end
end
