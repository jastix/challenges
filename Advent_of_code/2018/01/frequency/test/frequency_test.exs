defmodule FrequencyTest do
  use ExUnit.Case
  doctest Frequency

  test "computes demo frequency" do
    assert Frequency.compute(["+1\n", "-2\n", "+3\n", "+1"]) == 3
  end

  @tag timeout: 3000
  test "computes demo frequency 2" do
    assert Frequency.compute_2(["+1\n", "-2\n", "+3\n", "+1"]) == 2
  end
end
