defmodule Frequency do
  @moduledoc """
  Advent of code 2018 Day 1 solutions.
  """

  @doc """
  Computes frequencies.

  ## Examples

      iex> Frequency.compute(["1"])
      1

  """
  def main(args) do
    {opts, _, _} = OptionParser.parse(
      args,
      switches: [file: :string, part: :integer],
      aliases: [f: :file, p: :part]
    )
    input_file = File.stream!(opts[:file])
    cond do
      opts[:part] == 1 ->
        input_file |> compute()
      opts[:part] == 2 ->
        input_file |> compute_2()
    end
  end

  def compute(values) do
    values
    |> Stream.flat_map(&String.split(&1, "\n", trim: true))
    |> Stream.map(&String.to_integer(&1))
    |> Enum.sum()
    |> IO.inspect()
  end

  def compute_2(values) do
    values
    |> Stream.flat_map(&String.split(&1, "\n", trim: true))
    |> Stream.map(&String.to_integer(&1))
    |> Stream.cycle
    |> Enum.reduce_while(%{sum: 0, seen: %{}}, fn i, cache ->
      case Map.get(cache[:seen], cache[:sum] + i, 0) < 1 do
        true -> {:cont, update_map(cache, cache[:sum] + i)}
        false -> {:halt, cache[:sum] + i}
      end
    end)
    |> IO.inspect
  end

  defp update_map(map, key) do
    map = put_in(map[:sum], key)
    case get_in(map, [:seen, key]) == nil do
      true -> put_in(map[:seen][key], 1)
      false -> update_in(map[:seen][key], &(&1 + 1))
    end
  end

end
