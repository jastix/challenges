import sys

seen = []
matched = []


def find_similar(str, seen):
    seen.append(str)
    for s in seen:
        if is_similar(str, s):
            matched.append((str, s))
    return matched


def is_similar(str, s):
    num_diff = 0
    if str == s:
        return False
    for x in range(0, len(s)):
        if str[x] != s[x]:
            num_diff += 1
        if num_diff > 1:
            return False
    return True


def remove_similar_char(str, s):
    new_str = []
    for x in range(0, len(s)):
        if str[x] == s[x]:
            new_str.append(str[x])
    return ''.join(new_str)


filename = sys.argv[1]
with open(filename) as f:
    input = f.read()
    for s in input.splitlines():
        find_similar(s, seen)
res = find_similar(s, seen)

print(remove_similar_char(res[0][0], res[0][1]))
