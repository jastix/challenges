defmodule Checksum do
  @moduledoc """
  Documentation for Checksum.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Checksum.compute(["abcdeff"])
      1

  """
  def main(args) do
    {opts, _, _} = OptionParser.parse(
      args,
      switches: [file: :string, part: :integer],
      aliases: [f: :file, p: :part]
    )
    input_file = File.stream!(opts[:file])
    cond do
      opts[:part] == 1 ->
        input_file |> compute()
      # opts[:part] == 2 ->
      #   input_file |> compute_2()
    end
  end

  def compute(values) do
    values
    |> Stream.flat_map(&String.split(&1, "\n", trim: true))
    |> Stream.flat_map(fn a ->
      a
      |> String.graphemes()
      |> Enum.reduce(%{}, fn a, acc ->
        Map.update(acc, a, 1, &(&1 + 1))
      end)
      |> filter_map(fn _, v -> v > 1 end)
      |> Map.values
      |> Enum.uniq
    end)
    |> Enum.reduce(%{}, fn v, acc ->
      Map.update(acc, v, 1, &(&1 + 1))
    end)
    |> Map.values
    |> Enum.reduce(&*/2)
    |> IO.inspect
  end

  defp filter_map(map, fun) do
    :maps.filter fun, map
  end


  def compute_2(values) do
  end
end
