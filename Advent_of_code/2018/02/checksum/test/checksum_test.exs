defmodule ChecksumTest do
  use ExUnit.Case
  doctest Checksum

  test "calculates demo checksum" do
    input_data = ["abcdef\n", "bababc\n", "abbcde\n", "abcccd\n", "aabcdd\n", "abcdee\n", "ababab" ]
    assert Checksum.compute(input_data) == 12
  end
end
