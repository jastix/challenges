"""
https://adventofcode.com/2021/day/14
"""

import sys
from typing import List, Tuple, Deque, Dict
from collections import deque, Counter
from itertools import islice


def compute(*, filename) -> int:
    rules: Dict = {}
    init_str: str = ""
    for idx, line in enumerate(read_file(filename)):
        if idx == 0:
            init_str = line.strip()
        else:
            if line.strip() != "":
                rule_key, insert_val = line.strip().split(" -> ")
                rules[rule_key] = insert_val
    polymer = build_polymer(init_str, rules, n_steps=10)
    polymer_counts = Counter([ch for ch in polymer])
    min_elem = min(polymer_counts.values())
    max_elem = max(polymer_counts.values())
    result = max_elem - min_elem
    return result


def build_polymer(init_str, rules, n_steps) -> str:
    new_polymer: str = init_str
    for step in range(n_steps):
        print(step)
        step_polymer: List[str] = []
        for pair in sliding_window(new_polymer, 2):
            new_pair = []
            if pair in rules.keys():
                if len(step_polymer) == 0:
                    new_pair = [pair[0], rules[pair], pair[1]]
                else:
                    new_pair = [rules[pair], pair[1]]
            else:
                new_pair = [pair]
            step_polymer.append("".join(new_pair))
        new_polymer = "".join(step_polymer)

    return new_polymer


def sliding_window(iterable, n):
    # from Python docs recipes
    # sliding_window('ABCDEFG', 4) -> ABCD BCDE CDEF DEFG
    it = iter(iterable)
    window = deque(islice(it, n), maxlen=n)
    if len(window) == n:
        # yield tuple(window)
        yield "".join(tuple(window))
    for x in it:
        window.append(x)
        yield "".join(tuple(window))


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 5656
