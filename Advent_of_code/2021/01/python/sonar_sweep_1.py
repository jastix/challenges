def read_file(name):
    for row in open(name, "r"):
        yield row


def compute(*, filename) -> int:
    """Function to compute number of times a value increased
    relative to previous value
    """
    prev_val = None
    incs_counter = 0
    for val in read_file(filename):
        if prev_val:
            if int(val) > prev_val:
                incs_counter += 1
        prev_val = int(val)
    return incs_counter


if __name__ == "__main__":
    print(compute(filename="input.txt"))
