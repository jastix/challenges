def read_file(name):
    for row in open(name, "r"):
        yield row


def compute(*, filename) -> int:
    """Function to compute number of times a value increased
    relative to previous 3 values
    """
    prev_vals = list()
    prev_window_sum = None
    incs_counter = 0
    for val in read_file(filename):
        if prev_vals and len(prev_vals) >= 2:
            if len(prev_vals) == 3:
                prev_vals.pop(0)  # keeping only 3 elements in prev_vals
            prev_vals.append(int(val))
            curr_window_sum = sum(prev_vals)
            if prev_window_sum:
                if curr_window_sum > prev_window_sum:
                    incs_counter += 1
            prev_window_sum = curr_window_sum
        else:
            prev_vals.append(int(val))
    return incs_counter


if __name__ == "__main__":
    print(compute(filename="input.txt"))
