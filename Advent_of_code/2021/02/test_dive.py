from dive_1 import compute
from dive_2 import compute as compute_2

def test_sample_1():
  filename = 'input_sample.txt'
  assert compute(filename=filename) == 150


def test_sample_2():
  filename = 'input_sample.txt'
  assert compute_2(filename=filename) == 900