"""
https://adventofcode.com/2021/day/2
"""

import sys


class Submarine:
    """Class to hold submarine attributes and actions"""

    def __init__(self):
        self.horizon: int = 0
        self.depth: int = 0
        self.aim: int = 0

    def move(self, action, value):
        if action == "forward":
            self.horizon = self.horizon + value
            self.depth = self.depth + (self.aim * value)
        elif action == "down":
            self.aim = self.aim + value
        elif action == "up":
            self.aim = self.aim - value
        else:
            raise ValueError(f"Unknown action: {action}")

    def travel_distance(self) -> int:
        return self.horizon * self.depth


def compute(*, filename) -> int:
    """Function to compute depth and horizontal position"""
    subm = Submarine()
    for line in read_file(filename):
        action, value = line.strip().split()
        subm.move(action=action, value=int(value))
    return subm.travel_distance()


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 1845455714
