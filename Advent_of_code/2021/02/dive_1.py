"""
https://adventofcode.com/2021/day/2
"""

import sys


def read_file(name):
    for row in open(name, "r"):
        yield row


def compute(*, filename) -> int:
    """Function to compute depth and horizontal position
    """
    subm = {"horizon": 0, "depth": 0}
    for line in read_file(filename):
        action, value = line.strip().split()
        if action == 'forward':
            subm['horizon'] = subm['horizon'] + int(value)
        elif action == 'down':
            subm['depth'] = subm['depth'] + int(value)
        elif action == 'up':
            subm['depth'] = subm['depth'] - int(value)
        else:
            raise ValueError(f"Unknown action: {action}")
    result = subm['horizon'] * subm['depth']
    return result


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 1855814