"""
https://adventofcode.com/2021/day/13
"""

import sys
from typing import List, Tuple


def compute(*, filename) -> int:
    dot_coords: List[Tuple[str, str]] = list()
    folds: List[Tuple(str, int)] = list()
    max_x: int = 0
    max_y: int = 0
    for line in read_file(filename):
        curr_line = line.strip()
        if curr_line != "":
            if line.startswith("fold"):
                if len(folds) == 0:
                    axis, val = curr_line.split(" ")[-1].split("=")
                    folds.append((axis, int(val)))
            else:
                x, y = map(int, curr_line.split(","))
                dot_coords.append((x, y))
                if max_x < x:
                    max_x = x
                if max_y < y:
                    max_y = y
    init_grid = initialize_grid(dot_coords, max_x, max_y)
    folded = do_fold(init_grid, folds)
    num_dots = 0
    for line in folded:
        for ch in line:
            if ch == "#":
                num_dots += 1
    result = num_dots
    return result


def initialize_grid(coords, max_x, max_y):
    grid = list()
    x_elems = max_x + 1
    for y in range(0, max_y + 1):
        line = ["."] * x_elems
        for x, val in enumerate(line):
            if (x, y) in coords:
                line[x] = "#"
        grid.append(line)
    return grid


def do_fold(coords, folds) -> List[List[str]]:
    folded = coords
    for fold in folds:
        if fold[0] == "x":
            folded = fold_x(folded, fold[1])
        elif fold[0] == "y":
            folded = fold_y(folded, fold[1])
        else:
            raise ValueError(f"unknown fold: {fold}")
    return folded


def fold_x(coords, fold_line: int):
    folded_coord = list()
    y_elems = len(coords)
    x_elems = len(coords[0]) - 1
    for l in range(0, y_elems):
        new_row = ["."] * fold_line
        for c in range(0, fold_line):
            if "#" in (coords[l][x_elems - c], coords[l][c]):
                new_row[c] = "#"
        folded_coord.append(new_row)
    return folded_coord


def fold_y(coords, fold_line: int):
    folded_coord = list()
    max_y = len(coords) - 1
    x_elems = len(coords[0])
    for l in range(0, fold_line):
        new_line = ["."] * x_elems
        for c in range(0, x_elems):
            if "#" in (coords[max_y - l][c], coords[l][c]):
                new_line[c] = "#"
        folded_coord.append(new_line)
    return folded_coord


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 775
