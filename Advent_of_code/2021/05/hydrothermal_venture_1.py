"""
https://adventofcode.com/2021/day/5
https://www.reddit.com/r/adventofcode/comments/r9824c/2021_day_5_solutions/
"""

import sys
from typing import NamedTuple
from collections import Counter  # TIL


class Coord(NamedTuple):
    x1: int
    y1: int
    x2: int
    y2: int


def compute(*, filename) -> int:
    points: Counter = Counter()
    # input: x1,y1 -> x2,y2
    for line in read_file(filename):
        coord_pair = parse_coord(line)
        if is_horizontal_line(coord_pair):
            line_points = compute_line_points(coord_pair)
            points += line_points
    overlap_points_cnt = 0
    for val in points.values():
        if val >= 2:
            overlap_points_cnt += 1
    result = overlap_points_cnt
    return result


def compute_line_points(coord):
    if coord.x1 == coord.x2:
        points = [
            (coord.x1, y)
            for y in range(min(coord.y1, coord.y2), max(coord.y1, coord.y2) + 1)
        ]
    elif coord.y1 == coord.y2:
        points = [
            (x, coord.y1)
            for x in range(min(coord.x1, coord.x2), max(coord.x1, coord.x2) + 1)
        ]
    cnt_points = Counter(points)
    return cnt_points


def parse_coord(coord: str):
    coord_pair_str = coord.strip().split(" -> ")
    coord_int_pairs = (el.split(",") for el in coord_pair_str)
    coord_pairs = [tuple(map(int, el)) for el in coord_int_pairs]
    parsed_coord = Coord(
        x1=coord_pairs[0][0],
        x2=coord_pairs[1][0],
        y1=coord_pairs[0][1],
        y2=coord_pairs[1][1],
    )
    return parsed_coord


def is_horizontal_line(coords) -> bool:
    xs_equal = coords.x1 == coords.x2
    ys_equal = coords.y1 == coords.y2
    if xs_equal or ys_equal:
        return True
    return False


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 7297
