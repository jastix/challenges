"""
https://adventofcode.com/2021/day/5
https://www.reddit.com/r/adventofcode/comments/r9824c/2021_day_5_solutions/
"""

import sys
from typing import NamedTuple
from collections import Counter


class Coord(NamedTuple):
    x1: int
    y1: int
    x2: int
    y2: int


def compute(*, filename) -> int:
    points: Counter = Counter()
    # input: x1,y1 -> x2,y2
    for line in read_file(filename):
        coord_pair = parse_coord(line)
        line_points = compute_line_points(coord_pair)
        points += line_points
    overlap_points_cnt = 0
    for val in points.values():
        if val >= 2:
            overlap_points_cnt += 1
    result = overlap_points_cnt
    return result


def compute_line_points(coord: Coord) -> Counter:
    points = None
    if coord.x1 == coord.x2:
        points = [
            (coord.x1, y)
            for y in range(min(coord.y1, coord.y2), max(coord.y1, coord.y2) + 1)
        ]
    elif coord.y1 == coord.y2:
        points = [
            (x, coord.y1)
            for x in range(min(coord.x1, coord.x2), max(coord.x1, coord.x2) + 1)
        ]
    # diagonal line
    else:
        # compute x and y direction
        x_dir = 1 if coord.x1 < coord.x2 else -1
        y_dir = 1 if coord.y1 < coord.y2 else -1
        xs = range(coord.x1, coord.x2 + x_dir, x_dir)  # all x coordinates
        ys = range(coord.y1, coord.y2 + y_dir, y_dir)  # all y coordinates
        # in this case x and y number of diagonal points is the same
        points = list(zip(xs, ys))
    cnt_points = Counter(points)
    return cnt_points


def parse_coord(coord: str) -> Coord:
    coord_pair_str = coord.strip().split(" -> ")
    coord_pairs = [el.split(",") for el in coord_pair_str]
    parsed_coord = Coord(
        x1=int(coord_pairs[0][0]),
        x2=int(coord_pairs[1][0]),
        y1=int(coord_pairs[0][1]),
        y2=int(coord_pairs[1][1]),
    )
    return parsed_coord


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 21038
