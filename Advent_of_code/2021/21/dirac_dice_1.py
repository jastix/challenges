"""
https://adventofcode.com/2021/day/21
"""

import sys
from typing import List, Tuple
from itertools import islice, cycle


def compute(*, filename) -> int:
    start_pos: List[int] = list()
    dice_range = range(1, 101)
    for line in read_file(filename):
        if line.strip() != "":
            player_pos = int(line.strip().split(":")[1])
            start_pos.append(player_pos)
    lost_score, dice_cnt = roll_dice(start_pos, dice_range)
    result = lost_score * dice_cnt
    return result


def roll_dice(start_pos, dice_range) -> Tuple[int, int]:
    winning_score: int = 1000
    roll_cnt: int = 0
    scores: List[int] = [0, 0]
    dice = cycle(dice_range)
    while max(scores) < 1000:
        for idx, _ in enumerate(start_pos):
            dice_sum = sum(take(3, dice))
            roll_score = dice_sum + start_pos[idx]
            new_pos = roll_score % 10
            if new_pos == 0:
                new_pos = 10
            start_pos[idx] = new_pos
            scores[idx] += new_pos
            roll_cnt += 3
            if scores[idx] >= winning_score:
                break
    result = (min(scores), roll_cnt)
    return result


def take(n, iterable):
    "Return first n items of the iterable as a list"
    "from Python Docs Itertools Recipes"
    return list(islice(iterable, n))


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 913560
