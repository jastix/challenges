"""
https://adventofcode.com/2021/day/7
"""

import sys
from typing import List
from statistics import mean
import math


def compute(*, filename) -> int:
    crabs: List[int] = []
    for idx, line in enumerate(read_file(filename)):
        if idx == 0:
            crabs = list(map(int, line.strip().split(",")))
        else:
            break
    max_sub = math.ceil(mean(crabs))
    min_sub = min(crabs)
    min_sum = None
    for i in range(min_sub, max_sub + 1):
        diffs = (cr - i for cr in crabs)
        diff_sum = sum(abs(v) for v in diffs)
        if min_sum:
            min_sum = min(min_sum, diff_sum)
        else:
            min_sum = diff_sum
    result = min_sum
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 341534
