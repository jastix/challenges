"""
https://adventofcode.com/2021/day/4
"""

import sys
from typing import List


def compute(*, filename) -> int:
    draw_nums: List[int] = list()
    boards: List[List[List[int]]] = list()
    curr_card: List[List[int]] = list()
    for idx, line in enumerate(read_file(filename)):
        if idx == 0:
            draw_nums = list(map(int, line.strip().split(",")))
        else:
            if line.strip() != "":
                curr_line = list(map(int, line.strip().split()))
                curr_card.append(curr_line)
                if len(curr_card) == 5:  # bingo board has size 5x5
                    boards.append(curr_card)
                    curr_card = list()
    win_num, win_board = make_draw(draw_nums, boards)
    board_sum = sum_winning_board(win_board)
    result = win_num * board_sum
    return result


def make_draw(draw_nums, boards):
    """Function that computes last winning board and last drawn number
    Substitutes drawn number with -1 in boards
    """
    play_boards = boards
    win_num = None
    last_winning_board = None
    won_boards = set()
    is_row_marked: bool = False
    is_column_marked: bool = False
    for num in draw_nums:
        for b_idx, board in enumerate(play_boards):
            if b_idx in won_boards:
                continue
            for l_idx, line in enumerate(board):
                for el_idx, el in enumerate(line):
                    if el == num:
                        play_boards[b_idx][l_idx][el_idx] = -1
                        is_row_marked = all(
                            el == -1 for el in play_boards[b_idx][l_idx]
                        )
                        is_column_marked = all(
                            ell[el_idx] == -1 for ell in play_boards[b_idx]
                        )
                if is_row_marked or is_column_marked:
                    won_boards.add(b_idx)
                    is_row_marked = False
                    is_column_marked = False
            if len(won_boards) == len(play_boards):
                win_num = num
                last_winning_board = play_boards[b_idx]
                return (win_num, last_winning_board)
    return None


def sum_winning_board(win_board) -> int:
    total: int = 0
    for line in win_board:
        line_sum = sum(el for el in line if el > -1)
        total += line_sum
    return total


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 16168
