"""
https://adventofcode.com/2021/day/9
"""

import sys
from typing import List


def compute(*, filename) -> int:
    tubes: List[List[int]] = []
    for line in read_file(filename):
        tube = list(int(ch) for ch in line.strip())
        tubes.append(tube)
    low_points = []
    for idx_r, row in enumerate(tubes):
        for idx_c, col in enumerate(row):
            adjacent_points = []
            if idx_r == 0 and idx_c == 0:  # top-left corner
                adjacent_points = [row[idx_c + 1], tubes[idx_r + 1][idx_c]]
            elif idx_r == 0 and idx_c < len(row) - 1:  # top row
                adjacent_points = [row[idx_c - 1], row[idx_c + 1], tubes[idx_r + 1][idx_c]]
            elif idx_r == 0 and idx_c == len(row) - 1: # top-right corner
                adjacent_points = [row[idx_c - 1], tubes[idx_r + 1][idx_c]]
            elif idx_r == (len(tubes) - 1) and idx_c == 0:  # bottom left corner
                adjacent_points = [row[idx_c + 1], tubes[idx_r - 1][idx_c]]
            elif idx_r == (len(tubes) - 1) and idx_c == len(row) - 1:  # bottom right corner
                adjacent_points = [row[idx_c - 1], tubes[idx_r - 1][idx_c]]
            elif idx_r == len(tubes) - 1 and idx_c < len(row) - 1:  # bottom row
                adjacent_points = [row[idx_c - 1], tubes[idx_r - 1][idx_c], row[idx_c + 1]]
            elif idx_c == 0:  # first column
                adjacent_points = [row[idx_c + 1], tubes[idx_r - 1][idx_c], tubes[idx_r + 1][idx_c]]
            elif idx_c == len(row) - 1:  # last column
                adjacent_points = [row[idx_c - 1], tubes[idx_r - 1][idx_c], tubes[idx_r + 1][idx_c]]
            else:  # middle values
                adjacent_points = [row[idx_c - 1], row[idx_c + 1], tubes[idx_r - 1][idx_c], tubes[idx_r + 1][idx_c]]
            if all(col < point for point in adjacent_points):
                low_points.append(col)

    result = sum(i + 1 for i in low_points)
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 452
