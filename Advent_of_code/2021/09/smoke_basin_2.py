"""
https://adventofcode.com/2021/day/9
"""

import sys
from typing import Deque, List, Set, Tuple
from collections import deque
import math

# possible directions (row, column)
directions: List[Tuple[int, int]] = [
    (0, 1),  # next value right
    (0, -1),  # next value to left
    (1, 0),  # next value top
    (-1, 0),  # next value bottom
]


def compute(*, filename) -> int:
    tubes: List[List[int]] = []
    for line in read_file(filename):
        tube = list(int(ch) for ch in line.strip())
        tubes.append(tube)
    basin_sizes: List[int] = []
    grid_width = len(tubes[0])
    grid_height = len(tubes)
    for idx_r, row in enumerate(tubes):
        for idx_c, col in enumerate(row):
            curr_coord = (idx_r, idx_c)
            adjacent_coords = valid_adjacent_coords(curr_coord, grid_width, grid_height)
            adjacent_points = [
                tubes[coords[0]][coords[1]] for coords in adjacent_coords
            ]
            if all(col < point for point in adjacent_points):
                # we are in a low point
                low_point = (idx_r, idx_c)
                low_point_basin_size = fill_basin(
                    low_point, tubes, grid_width, grid_height
                )
                basin_sizes.append(low_point_basin_size)

    result = math.prod(sorted(basin_sizes)[-3:])
    return result


def fill_basin(init_coord, grid, grid_width: int, grid_height: int) -> int:
    """Function that moves from initial coordinate vertically and horizontally in 1 step increments
    until cell with value 9 or edge of grid is encountered"""
    basin_points: Set[Tuple[int, int]] = set()
    possible_moves: Deque[Tuple[int, int]] = deque()
    init_coords = valid_adjacent_coords(init_coord, grid_width, grid_height)
    for coord in init_coords:
        if grid[coord[0]][coord[1]] < 9:
            possible_moves.append(coord)
            basin_points.add(coord)
    while possible_moves:
        new_coord = possible_moves.pop()
        adjacent_coords = valid_adjacent_coords(new_coord, grid_width, grid_height)
        for coord in adjacent_coords:
            if grid[coord[0]][coord[1]] < 9:
                if coord not in basin_points:
                    basin_points.add(coord)
                    possible_moves.append(coord)
    return len(basin_points)


def valid_adjacent_coords(
    from_coord, grid_width: int, grid_height: int
) -> List[Tuple[int, int]]:
    """Function that returns coordinates that are on the grid
    starting from some initial coordinate"""
    valid_points: List[Tuple[int, int]] = []
    for direction in directions:
        coord = (from_coord[0] + direction[0], from_coord[1] + direction[1])
        if coord[0] >= 0 and coord[1] >= 0:
            if coord[0] < grid_height and coord[1] < grid_width:
                valid_points.append(coord)
    return valid_points


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 1263735
