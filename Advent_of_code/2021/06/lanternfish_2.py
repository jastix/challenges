"""
https://adventofcode.com/2021/day/6
https://www.reddit.com/r/adventofcode/comments/r9z49j/2021_day_6_solutions/
"""

import sys
from typing import List
from collections import Counter


def compute(*, filename, iter_days: int = 256) -> int:
    fishes: List[int] = []
    for idx, line in enumerate(read_file(filename)):
        if idx == 0:
            fishes = list(map(int, line.strip().split(",")))
        else:
            break
    total_fish_cnt = simulate_fish(fishes, iter_days)
    result = total_fish_cnt
    return result


def simulate_fish(init_fishes, iter_days: int) -> int:
    """Simulates fish population by state counting instead of materializing every state over time"""
    fishes = Counter([f for f in init_fishes])
    for day in range(iter_days):
        new_f = fishes[0]
        for state in range(8):
            fishes[state] = fishes[state + 1]  # fish count moves to the next state
        fishes[8] = new_f  # fishes that were in 0 state spawned fishes in 8
        fishes[6] += new_f  # fishes in 0 state moved to state 6

    result = sum(fishes.values())
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 1569108373832
