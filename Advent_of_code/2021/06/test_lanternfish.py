from lanternfish_1 import compute
from lanternfish_2 import compute as compute_2


def test_sample_1():
  filename = 'input_sample.txt'
  iter_days = 80
  assert compute(filename=filename, iter_days=iter_days) == 5934


def test_sample_2():
  filename = 'input_sample.txt'
  iter_days = 80
  assert compute_2(filename=filename, iter_days=iter_days) == 5934