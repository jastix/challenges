"""
https://adventofcode.com/2021/day/6
"""

import sys
from typing import List
from collections import Counter


def compute(*, filename, iter_days=256) -> int:
    iter_days = iter_days
    fishes: List[int] = []
    for idx, line in enumerate(read_file(filename)):
        if idx == 0:
            fishes = list(map(int, line.strip().split(",")))
        else:
            break
    total_fish_cnt = simulate_fish(fishes, iter_days)
    result = total_fish_cnt
    return result


def simulate_fish(fishes, iter_days: int) -> int:
    while iter_days > 0:
        new_fishes = []
        for idx, fish in enumerate(fishes):
            if fish == 0:
                fishes[idx] = 6
                new_fishes.append(8)
            else:
                fishes[idx] -= 1
        if new_fishes:
            fishes += new_fishes
        iter_days -= 1
        print(iter_days)
    result = len(fishes)
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 343441
