"""
https://adventofcode.com/2021/day/12
"""

import sys
from typing import List, Tuple, Deque, Dict
from collections import deque


def compute(*, filename) -> int:
    edges: List[Tuple[str, str]] = list()
    edge_dict: Dict = {}
    for line in read_file(filename):
        start, end = line.strip().split("-")
        edges.append((start, end))
        if end == "end" or start == "start":
            if start not in edge_dict.keys():
                edge_dict[start] = [end]
            else:
                edge_dict[start].append(end)
        elif end == "start":
            if end not in edge_dict.keys():
                edge_dict[end] = [start]
            else:
                edge_dict[end].append(start)
        else:
            if start not in edge_dict.keys():
                edge_dict[start] = [end]
            else:
                edge_dict[start].append(end)
            if end not in edge_dict.keys():
                edge_dict[end] = [start]
            else:
                edge_dict[end].append(start)

    result = len(traverse(edge_dict))
    return result


def traverse(edge_dict) -> List[List[str]]:
    complete_paths: List[List[str]] = []
    possible_moves: Deque = deque()
    for edge in edge_dict["start"]:
        possible_moves.append(["start", edge])
    while possible_moves:
        curr_path = possible_moves.popleft()
        for edge in edge_dict[curr_path[-1]]:
            if edge == "end":
                complete_paths.append(curr_path + [edge])
            else:
                visited_small_caves = set([ch for ch in curr_path if ch.islower()])
                if edge not in visited_small_caves:
                    possible_moves.append(curr_path + [edge])
    return complete_paths


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 4754
