from passage_passing_1 import compute
from passage_passing_2 import compute as compute_2


# part 1 tests
def test_sample_1_1():
    filename = "input_sample.txt"
    assert compute(filename=filename) == 10


def test_sample_1_2():
    filename = "input_sample_2.txt"
    assert compute(filename=filename) == 19


def test_sample_1_3():
    filename = "input_sample_3.txt"
    assert compute(filename=filename) == 226


# part 2 tests
def test_sample_2_1():
    filename = "input_sample.txt"
    assert compute_2(filename=filename) == 36


def test_sample_2_2():
    filename = "input_sample_2.txt"
    assert compute_2(filename=filename) == 103


def test_sample_2_3():
    filename = "input_sample_3.txt"
    assert compute_2(filename=filename) == 3509
