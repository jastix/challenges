"""
https://adventofcode.com/2021/day/12
"""

import sys
from typing import List, Tuple, Deque, Dict
from collections import deque, Counter


def compute(*, filename) -> int:
    edges: List[Tuple[str, str]] = list()
    edge_dict: Dict = {}
    for line in read_file(filename):
        start, end = line.strip().split("-")
        edges.append((start, end))
        "need to ensure here that 'start' is not in destinations"
        if end == "end" or start == "start":
            if start not in edge_dict.keys():
                edge_dict[start] = [end]
            else:
                edge_dict[start].append(end)
        elif end == "start":
            if end not in edge_dict.keys():
                edge_dict[end] = [start]
            else:
                edge_dict[end].append(start)
        else:
            if start not in edge_dict.keys():
                edge_dict[start] = [end]
            else:
                edge_dict[start].append(end)
            if end not in edge_dict.keys():
                edge_dict[end] = [start]
            else:
                edge_dict[end].append(start)
    result = traverse(edge_dict)
    return result


def traverse(edge_dict) -> int:
    """now allowed to visit one small cave twice"""
    num_complete_paths: int = 0
    possible_moves: Deque = deque()
    for edge in edge_dict["start"]:
        possible_moves.append(["start", edge])
    while possible_moves:
        curr_path = possible_moves.pop()
        for edge_dest in edge_dict[curr_path[-1]]:
            if edge_dest == "end":
                num_complete_paths += 1
            else:
                if edge_dest.islower():
                    if edge_dest not in curr_path:
                        possible_moves.append(curr_path + [edge_dest])
                    else:
                        # if this not visited twice
                        # and any other small destination as well
                        visited_small_caves: Counter = Counter(
                            [ch for ch in curr_path if ch.islower()]
                        )
                        if 2 not in visited_small_caves.values():
                            possible_moves.append(curr_path + [edge_dest])
                else:
                    possible_moves.append(curr_path + [edge_dest])
    return num_complete_paths


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 143562
