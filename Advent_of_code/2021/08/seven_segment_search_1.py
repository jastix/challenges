"""
https://adventofcode.com/2021/day/8
"""

import sys


displays = (
    # a,b,c,d,e,f,g
    "1110111",  # 0
    "0010010",  # 1
    "1011101",  # 2
    "1011011",  # 3
    "0111010",  # 4
    "0101011",  # 5
    "1101111",  # 6
    "1010010",  # 7
    "1111111",  # 8
    "1111011",  # 9
)

displays_d = {
    # '1110111': 0,
    "0010010": 1,
    # '1011101': 2,
    # '1011011': 3,
    "0111010": 4,
    # '0101011': 5,
    # '1101111': 6,
    "1010010": 7,
    "1111111": 8,
    # '1111011': 9
}


def compute(*, filename) -> int:
    digits_segments = {}
    digits_count = 0
    for k, v in displays_d.items():
        k_segments = sum(int(ch) for ch in k)
        digits_segments[k_segments] = v
    for line in read_file(filename):
        inputs = line.strip().split("|")
        output = inputs[1].strip().split(" ")
        line_segments = [len(l) for l in output]
        for line_segment in line_segments:
            if line_segment in digits_segments.keys():
                digits_count += 1
    result = digits_count
    return result


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 255
