"""
https://adventofcode.com/2021/day/3
"""

import sys


def compute(*, filename) -> int:
    nums = list()
    for line in read_file(filename):
        nums.append(line.strip())
    ogr = compute_rating(nums, "oxygen")
    csr = compute_rating(nums, "co2")
    result = ogr * csr
    return result


def oxygen_criteria(cnt):
    if cnt[0] > cnt[1]:
        bit_criteria = 0
    else:
        bit_criteria = 1
    return bit_criteria


def co2_criteria(cnt):
    if cnt[0] <= cnt[1]:
        bit_criteria = 0
    else:
        bit_criteria = 1
    return bit_criteria


def compute_rating(nums, type):
    """Function that computes rating based in bit criteria
    and filtering of values that do not match the bit criteria
    Result is returned as decimal value
    """
    init_nums = nums
    num_len = len(init_nums[0])
    for idx in range(num_len):
        cnt = [0, 0]
        for num in init_nums:
            nth_digit = int(num[idx])
            if nth_digit == 0:
                cnt[0] += 1
            else:
                cnt[1] += 1
        if type == "oxygen":
            bit_criteria = oxygen_criteria(cnt)
        elif type == "co2":
            bit_criteria = co2_criteria(cnt)
        else:
            raise ValueError(f"Unknown type: {type}")
        bad_vals = []
        for num in init_nums:
            nth_digit = int(num[idx])
            if nth_digit != bit_criteria:
                bad_vals.append(num)
        init_nums = list(filter(lambda v: v not in bad_vals, init_nums))
        if len(init_nums) == 1:
            break
    rating = init_nums[0]
    decimal_rate = int(rating, 2)
    return decimal_rate


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 3368358
