"""
https://adventofcode.com/2021/day/3
"""

import sys


def compute(*, filename) -> int:
    """Function to compute depth and horizontal position"""
    counts = list() # implicit indexes. ( (0, 1), () )
    for line in read_file(filename):
        for idx, val in enumerate(line.strip()):
            val = int(val)
            if counts and len(counts) > idx:
                curr_cnt = counts[idx]
                if val in curr_cnt.keys():
                    curr_cnt[val] += 1
                else:
                    curr_cnt[val] = 1
            else:
               idx_cnt = {}
               idx_cnt[int(val)] = 1
               counts.append(idx_cnt) 
    gr = gamma_rate(counts)
    er = epsilon_rate(counts) 
    result = gr * er
    return result


def gamma_rate(bit_counts):
    common_bits = []
    for cnt in bit_counts:
        bit = max(cnt, key=cnt.get)
        common_bits.append(bit)
    bits_str = ''.join(str(b) for b in common_bits)
    decimal_rate = int(bits_str, 2)
    return decimal_rate


def epsilon_rate(bit_counts):
    common_bits = []
    for cnt in bit_counts:
        bit = min(cnt, key=cnt.get)
        common_bits.append(bit)
    bits_str = ''.join(str(b) for b in common_bits)
    decimal_rate = int(bits_str, 2)
    return decimal_rate



def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 2972336 
