from binary_diagnostic_1 import compute
from binary_diagnostic_2 import compute as compute_2


def test_sample_1():
    filename = "input_sample.txt"
    assert compute(filename=filename) == 198


def test_sample_2():
    filename = "input_sample.txt"
    assert compute_2(filename=filename) == 230
