"""
https://adventofcode.com/2021/day/10
"""

import sys
from typing import List, Optional


lead_chars = set(("(", "[", "{", "<"))
# mapping of closing character to opening char
chars_map = {")": "(", "]": "[", "}": "{", ">": "<"}
scoring = {")": 3, "]": 57, "}": 1197, ">": 25137}


def compute(*, filename) -> int:
    illegal_chars: List[str] = list()
    for line in read_file(filename):
        illegal_char = analyse_string(line.strip())
        if illegal_char:
            illegal_chars.append(illegal_char)
    result = sum(scoring[ch] for ch in illegal_chars)
    return result


def analyse_string(inp: str) -> Optional[str]:
    seen_chars = list()
    illegal_char = None
    for ch in inp:
        if ch in lead_chars:
            seen_chars.append(ch)
        else:  # not opening character
            if chars_map[ch] == seen_chars[-1]:
                seen_chars.pop(-1)  # removes last element from list
            else:
                illegal_char = ch
                return illegal_char
    return illegal_char


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 392097
