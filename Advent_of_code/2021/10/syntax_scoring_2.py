"""
https://adventofcode.com/2021/day/10
"""

import sys
from typing import List, Optional
from statistics import median


# mapping of closing character to opening char
chars_map = {")": "(", "]": "[", "}": "{", ">": "<"}
# mapping of opening characters to closing char
reverse_chars_map = {"(": ")", "[": "]", "{": "}", "<": ">"}
missing_scoring = {")": 1, "]": 2, "}": 3, ">": 4}


def compute(*, filename) -> int:
    fill_chars: List[List[str]] = list()
    for line in read_file(filename):
        line_fill_chars = analyse_string(line.strip())
        if line_fill_chars:
            fill_chars.append(line_fill_chars)
    missing_scores = (compute_str_score(chars) for chars in fill_chars)
    middle_score = median(missing_scores)
    result = int(middle_score)
    return result


def compute_str_score(inp: List[str]) -> int:
    score = 0
    for ch in inp:
        score *= 5
        score += missing_scoring[ch]
    return score


def analyse_string(inp) -> Optional[List[str]]:
    seen_chars = list()
    for ch in inp:
        if ch in reverse_chars_map.keys():
            seen_chars.append(ch)
        else:  # not opening character
            if chars_map[ch] == seen_chars[-1]:
                seen_chars.pop(-1)
            else:  # invalid string
                return None
    if seen_chars:
        fill_chars = [reverse_chars_map[ch] for ch in reversed(seen_chars)]
    return fill_chars


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 4263222782
