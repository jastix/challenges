"""
https://adventofcode.com/2021/day/11
"""

import sys
from typing import List, Tuple, Deque
from collections import deque

# possible directions (row, column)
directions: List[Tuple[int, int]] = [
    (0, 1),  # next value right
    (0, -1),  # next value to left
    (1, 0),  # next value top
    (-1, 0),  # next value bottom
    (1, 1),  # diagonal bottom-right
    (1, -1),  # diagonal bottom-left
    (-1, -1),  # diagonal top-left
    (-1, 1),  # diagonal top-right
]


def compute(*, filename, iter_cnt=100) -> int:
    octopuses: List[List[int]] = list()
    for line in read_file(filename):
        int_line = list(map(int, line.strip()))
        octopuses.append(int_line)
    flashes = simulate(octopuses, iter_cnt=iter_cnt)
    result = flashes
    return result


def simulate(octopuses: List[List[int]], iter_cnt: int) -> int:
    flashes_cnt = 0
    grid_width = len(octopuses[0])
    grid_height = len(octopuses)
    while iter_cnt > 0:
        curr_flashes: Deque[Tuple[int, int]] = deque()  # coordinates of flashes
        for idx_r, row in enumerate(octopuses):
            for idx_c, col in enumerate(row):
                octopuses[idx_r][idx_c] += 1
                if octopuses[idx_r][idx_c] == 10:
                    curr_flashes.append((idx_r, idx_c))
                    flashes_cnt += 1
        while curr_flashes:
            flash = curr_flashes.popleft()
            valid_coords = compute_valid_coords(flash, grid_width, grid_height)
            for coord in valid_coords:
                octopuses[coord[0]][coord[1]] += 1
                if octopuses[coord[0]][coord[1]] == 10:
                    curr_flashes.append((coord[0], coord[1]))
                    flashes_cnt += 1
        iter_cnt -= 1
        # reset flashes
        for idx_r, row in enumerate(octopuses):
            for idx_c, col in enumerate(row):
                if octopuses[idx_r][idx_c] >= 10:
                    octopuses[idx_r][idx_c] = 0
    return flashes_cnt


def compute_valid_coords(
    init_coord: Tuple[int, int], width: int, height: int
) -> List[Tuple[int, int]]:
    possible_coords = map(
        lambda d: (d[0] + init_coord[0], d[1] + init_coord[1]), directions
    )
    valid_coords = []
    for coords in possible_coords:
        if coords[0] >= 0 and coords[1] >= 0:  # coordinates are not negative
            if coords[0] < width and coords[1] < height:  # within grid
                valid_coords.append((coords[0], coords[1]))
    return valid_coords


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 1615
