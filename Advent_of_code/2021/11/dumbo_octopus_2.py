"""
https://adventofcode.com/2021/day/11
"""

import sys
from typing import List, Tuple, Deque, Set
from collections import deque

# possible directions (row, column)
directions: List[Tuple[int, int]] = [
    (0, 1),  # next value right
    (0, -1),  # next value to left
    (1, 0),  # next value top
    (-1, 0),  # next value bottom
    (1, 1),  # diagonal bottom-right
    (1, -1),  # diagonal bottom-left
    (-1, -1),  # diagonal top-left
    (-1, 1),  # diagonal top-right
]


def compute(*, filename) -> int:
    octopuses: List[List[int]] = list()
    for line in read_file(filename):
        int_line = list(map(int, line.strip()))
        octopuses.append(int_line)
    result = simulate(octopuses)
    return result


def simulate(octopuses: List[List[int]]) -> int:
    grid_width: int = len(octopuses[0])
    grid_height: int = len(octopuses)
    total_elements: int = grid_width * grid_height
    curr_iter_num: int = 1
    while True:
        curr_flashes: Deque[Tuple[int, int]] = deque()  # coordinates of new flashes
        flashed_coords: Set[Tuple[int, int]] = set()  # all unique flashed coords
        for idx_r, row in enumerate(octopuses):
            for idx_c, col in enumerate(row):
                octopuses[idx_r][idx_c] += 1
                if octopuses[idx_r][idx_c] == 10:
                    curr_flashes.append((idx_r, idx_c))
                    flashed_coords.add((idx_r, idx_c))
        while curr_flashes:
            flash = curr_flashes.popleft()
            valid_coords = compute_valid_coords(flash, grid_width, grid_height)
            for coord in valid_coords:
                octopuses[coord[0]][coord[1]] += 1
                # newly flashed octopus
                if (
                    octopuses[coord[0]][coord[1]] == 10
                    and (coord[0], coord[1]) not in flashed_coords
                ):
                    curr_flashes.append((coord[0], coord[1]))
                    flashed_coords.add((coord[0], coord[1]))
        # reset flashes
        for flash_coord in flashed_coords:
            octopuses[flash_coord[0]][flash_coord[1]] = 0
        # if number of unique coordinates flashing is the same as all elements on the grid
        # then the whole grid was flashing
        if len(flashed_coords) == total_elements:
            break
        curr_iter_num += 1
    return curr_iter_num


def compute_valid_coords(
    init_coord: Tuple[int, int], width: int, height: int
) -> List[Tuple[int, int]]:
    possible_coords = map(
        lambda d: (d[0] + init_coord[0], d[1] + init_coord[1]), directions
    )
    valid_coords = []
    for coords in possible_coords:
        if coords[0] >= 0 and coords[1] >= 0:  # coordinates are not negative
            if coords[0] < width and coords[1] < height:  # within grid
                valid_coords.append((coords[0], coords[1]))
    return valid_coords


def read_file(name):
    for row in open(name, "r"):
        yield row


if __name__ == "__main__":
    filename = "input_sample.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    print(compute(filename=filename))
# result: 249
