-- Weather Observation Station 8
-- MySQL
SELECT city
FROM station
WHERE city REGEXP '^[aeiou]' AND city REGEXP '[aeiou]$'
GROUP BY 1;