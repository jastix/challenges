-- Weather Observation Station 11
-- MySQL

SELECT city
FROM station
WHERE NOT city REGEXP '^[aeiou]' OR NOT city REGEXP '[aeiou]$'
GROUP BY 1;