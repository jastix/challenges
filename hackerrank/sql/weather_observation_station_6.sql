-- Weather Observation Station 6
-- MySQL
SELECT city
FROM station
WHERE city REGEXP '^[aeiou]'
GROUP BY 1;