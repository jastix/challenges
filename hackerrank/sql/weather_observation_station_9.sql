-- Weather Observation Station 9
-- MySQL
SELECT city
FROM station
WHERE NOT city REGEXP '^[aeiou]'
GROUP BY 1;