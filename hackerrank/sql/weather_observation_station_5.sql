-- MySQL 5.7
SELECT city, city_length
FROM (
    SELECT city, LENGTH(city) AS city_length
    FROM station
    ORDER BY 2 ASC, 1
    LIMIT 1) AS t
UNION ALL
SELECT city, city_length
FROM (
    SELECT city, LENGTH(city) AS city_length
    FROM station
    ORDER BY 2 DESC, 1
    LIMIT 1) AS t
;
