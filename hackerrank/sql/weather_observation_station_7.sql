-- Weather Observation Station 7
-- MySQL
SELECT city
FROM station
WHERE city REGEXP '[aeiou]$'
GROUP BY 1;