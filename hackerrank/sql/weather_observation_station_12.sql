-- Weather Observation Station 12
-- MySQL
SELECT city
FROM station
WHERE NOT city REGEXP '^[aeiou]' AND NOT city REGEXP '[aeiou]$'
GROUP BY 1;