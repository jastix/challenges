-- Weather Observation Station 10
-- MySQL
SELECT city
FROM station
WHERE NOT city REGEXP '[aeiou]$'
GROUP BY 1;