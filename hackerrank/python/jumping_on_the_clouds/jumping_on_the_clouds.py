#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the jumpingOnClouds function below.
def jumpingOnClouds(c):
    jumps = 0
    pos = 0
    while True:
        offset = 1
        if len(c) > pos + 2:
            if c[pos + 2] != 1:
                offset = 2
        elif len(c) > pos + 1:
            offset = 1
        else:
            break
        pos = pos + offset
        jumps += 1
    return jumps


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + '\n')

    fptr.close()