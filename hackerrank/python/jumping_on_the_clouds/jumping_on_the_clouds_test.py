import pytest
from jumping_on_the_clouds import jumpingOnClouds

def test_jumping_clouds_sample_0():
    inp = '0 0 1 0 0 1 0'
    c = list(map(int, inp.rstrip().split()))
    assert jumpingOnClouds(c) == 4

def test_jumping_clouds_sample_1():
    inp = '0 0 0 0 1 0'
    c = list(map(int, inp.rstrip().split()))
    assert jumpingOnClouds(c) == 3