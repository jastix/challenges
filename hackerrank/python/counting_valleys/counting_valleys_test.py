import pytest
from counting_valleys import countingValleys

def test_counting_valleys_sample_1():
    n = 8
    s = 'UDDDUDUU'
    assert countingValleys(n, s) == 1

def test_counting_valleys_sample_2():
    n = 12
    s = 'DDUUDDUDUUUD'
    assert countingValleys(n, s) == 2


def test_counting_valleys_case_2():
    n = 10
    s = 'UDUUUDUDDD'
    assert countingValleys(n, s) == 0


def test_counting_valleys_case_10():
    n = 1000
    s = 'UDDDUUDUUDDDDUUDDUDDDUUDDDUUDDUUDUDUDDDDUUDDDDDDUUUDUDUDUUDUUUUUUUUUUUDUUUUUDUDUDUDDDDUUDUUUDDDDDUUDUUDUUUUUUDDDDDUUDUUDDDUDUUUUUUUDUUUUDDUDDDDDUUUDDUUUUUUUDDDDUUUDUUUDUUUUUUUDDUUUDDDDUUDUDDDUDDDDUUDDDDDUUDUDDUDUUUDDUDUDDDUUDDUDUUUUUDDUUUDUUUUUUDDUDUUDDUUDDDUDUDDDDUDUDDUUUDDDUDDUDDUUUUDDDUDUDDDUUUUUDUUUDDUDUUDUUUDDUDUUUDUDUDUUUUDDUDDDUUUDDDUDUDDDDDDDUUUUDDUUDUUUDUDUDUDDUUUDDUDUDDUDDUDDDUDUDUDDUUDDDUUDUUDUDDDDDDDDUUDDUUDDUUUUUUDUUUDDUUDUDDUUDDDUDDUUUUUDUDUDDDUUUDDUUDDDUDUUDUUDDDUDDUUDUUUDDUUUUDUUUDDDDUDUUUDDUDDDDDDDUDDDUUDUDDUDUDUUDUDDUUDUUDUUDUUDUDDDUUDDUUUUDUUDUUDUUUUDUUUDDUUUDDUDUDDUDUDUUUDUDDDUUUUDUUDUUUDDUUDUDUDDDDUDUDDDDDUUUDUUUDUDDUUUDUUUUDDUUUDDDUDUDUUDUUUUUDUUDDDUUDDUDDDDUUUDUUUUUDDUUUUUDDUUDUDDUDUDUUUUDUUDUUUDUUDDDUUUUDDDUDUDUUDUDUUDDDUDDUUDDUDUDUUDUDUDDDUDDDUDDDDUDUUUUUDUDUDUUDUUDUDDUDUUDDUUDUDUDDDDUDDUDDUDDUUUUDUDDUUUUDUUDUDDUDDDUDUUUDUDUDUDDDUDDUUUUDDUDUDUDDUDDDUDDDUUUDUDUDUUDUUUUUDUDDDDUUDDDDUDUDDUDDDDUUUDUDUUDUDUUUDDUDUDDDDDUUDUDUUDDDDUUDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD'
    assert countingValleys(n, s) == 3