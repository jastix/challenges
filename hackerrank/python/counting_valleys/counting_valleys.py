#!/bin/python3

import math
import os
import random
import re
import sys

def detect_valleys(route):
    valleys = []
    is_valley = False
    started = False
    for v in route:
        if v < 1 and started is False:
            started = True
            is_valley = True
        elif started and v > 0:
            started = False
        elif started and v < 0:
            is_valley = True
        elif v == 0 and started:
            valleys.append(is_valley)
            started = False
        else:
            print(v)
    return valleys
    
def compute_route(steps):
    last_step = 0
    route = []
    for step in steps:
        if step == 'U':
            curr = last_step + 1
        elif step == 'D':
            curr = last_step - 1
        last_step = curr
        route.append(curr)
    return route

# Complete the countingValleys function below.
def countingValleys(n, s):
    route = compute_route(s)
    valleys = detect_valleys(route) 
    return len(valleys)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = countingValleys(n, s)

    fptr.write(str(result) + '\n')

    fptr.close()
