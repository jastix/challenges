import math
import os
import random
import re
import sys

def count_pairs(ar):
  colors = {}
  for sock in ar:
    if sock not in colors:
      colors[sock] = 1
    else:
      colors[sock] += 1
  return colors

# Complete the sockMerchant function below.
def sockMerchant(n, ar):
  color_socks = count_pairs(ar)
  print(color_socks)
  paired_colors = [v for k,v in color_socks.items() if v > 1 ]
  num_pairs = sum([int(v/2) for v in paired_colors])
  return num_pairs

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')

    fptr.close()