import pytest
from sock_merchant import sockMerchant

def test_merchant_sample():
    n = 9
    inp = "10 20 20 10 10 30 50 10 20"
    ar =  list(map(int, inp.rstrip().split()))
    assert sockMerchant(n, ar) == 3

def test_merchant_2():
    n = 20
    inp = "4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 5 5 5"
    ar =  list(map(int, inp.rstrip().split()))
    assert sockMerchant(n, ar) == 9