import pytest
from repeated_string import repeatedString

def test_sample_0():
    s = 'aba'
    n = 10
    assert repeatedString(s, n) == 7

def test_sample_1():
    s = 'a'
    n = 1000000000000
    assert repeatedString(s, n) == n

def test_case_7():
    s = 'kmretasscityylpdhuwjirnqimlkcgxubxmsxpypgzxtenweirknjtasxtvxemtwxuarabssvqdnktqadhyktagjxoanknhgilnm'
    n = 736778906400
    assert repeatedString(s, n) == 51574523448

def test_case_9():
    s = 'epsxyyflvrrrxzvnoenvpegvuonodjoxfwdmcvwctmekpsnamchznsoxaklzjgrqruyzavshfbmuhdwwmpbkwcuomqhiyvuztwvq'
    n = 549382313570
    assert repeatedString(s, n) == 16481469408