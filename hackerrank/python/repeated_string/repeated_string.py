#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the repeatedString function below.
def repeatedString(s, n):
    if s == 'a':
        return n
    if 'a' not in s:
        return 0
    parts = n // len(s)
    full_strings_count = s.count('a') * parts
    partial_count = s[:n % len(s)].count('a')
    res = full_strings_count + partial_count
    return res

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    n = int(input())

    result = repeatedString(s, n)

    fptr.write(str(result) + '\n')

    fptr.close()