defmodule Listdup do

  @doc """
  Ninety-Nine Problems
  P14 (*) Duplicate the elements of a list

  example:
  iex> Listdup.duplicate(["a","b","c","d"])
  ["a","a","b","b","c","c","d","d"]
  """

  def duplicate([]), do: []
  def duplicate([h | t]), do: [h, h] ++ duplicate(t)

  @doc """
  Ninety-Nine Problems
  P15 (**) Duplicate the elements of a list a given number of times

  example> Listdup.duplicate_n(["a","b","c"], 3)
  ["a","a","a","b","b","b","c","c","c"]
  """

  def duplicate_n([], _count    ), do: []
  def duplicate_n([h | t], count), do: _dup_list(h, count) ++ duplicate_n(t, count)

  defp _dup_list(_el, 0   ), do: []
  defp _dup_list(el, count), do: [el] ++ _dup_list(el, count - 1)
end
