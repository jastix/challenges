defmodule ListdupTest do
  use ExUnit.Case
  doctest Listdup

  test "it returns empty list when empty list is given" do
    assert Listdup.duplicate([]) == []
  end

  test "it duplicates one element list" do
    assert Listdup.duplicate(["a"]) == ["a", "a"]
  end

  test "it duplicates a list" do
    assert Listdup.duplicate(
      ["a", "b", 1, 2, "c"]
    ) == ["a", "a", "b", "b", 1, 1, 2, 2, "c", "c"]
  end

  ### Duplicate with count
  test "it returns empty list when empty list and count are given" do
    assert Listdup.duplicate_n([], 3) == []
  end

  test "it duplicates a one element list 3 times" do
    assert Listdup.duplicate_n(["a"], 3) == ["a", "a", "a"]
  end

  test "it duplicates a list given list and count" do
    assert Listdup.duplicate_n(
      ["a", 1, "c"], 4
    ) == ["a","a","a","a",1,1,1,1,"c","c","c","c"]
  end

end
