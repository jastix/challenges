defmodule ReverselistTest do
  use ExUnit.Case
  doctest Reverselist

  test "it returns empty list for empty list" do
    assert Reverselist.reverse([]) == []
  end

  test "it returns the same list if list has one element" do
    assert Reverselist.reverse([1]) == [1]
  end

  test "it returns reversed list" do
    assert Reverselist.reverse([1,2,3]) == [3,2,1]
  end
end
