defmodule Reverselist do
  @moduledoc """
  Ninety-Nine Problems
  P05 (*) Reverse a list

  example:
  iex> Reverselist.reverse([1,2,3,4])
  [4,3,2,1]
  """

  def reverse([]),    do: []
  def reverse([h|t]), do: reverse(t) ++ [h]
end
