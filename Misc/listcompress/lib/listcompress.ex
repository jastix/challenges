defmodule Listcompress do
  @moduledoc """
  Ninety-Nine Problems
  P08 (**) Eliminate consecutive duplicates of list elements
  Order of the elements must be preserved.

  example:
  iex> Listcompress.compress(["a","a",1,1,2])
  ["a",1,2]
  """

  def compress([]),             do: []
  def compress([h | [h | t]]),  do: compress([h | t])
  def compress([h | [ht | t]]), do: [h | compress([ht | t])]
  def compress([h | []]),       do: [h]
end
