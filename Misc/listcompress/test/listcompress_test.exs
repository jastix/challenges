defmodule ListcompressTest do
  use ExUnit.Case
  doctest Listcompress

  test "it returns empty list is empty list is given" do
    assert Listcompress.compress([]) == []
  end

  test "it returns the same list for one element list" do
    assert Listcompress.compress(["b"]) == ["b"]
  end

  test "it eliminates duplicates from the list" do
    assert Listcompress.compress([
      "a", "a", "a", "a", "b",
      "c", "c", "a", "a", "d",
      "e", "e", "e", "e"
    ]) == ["a", "b", "c", "a", "d", "e"]
  end

  test "it eliminates duplicates from mixed list" do
    assert Listcompress.compress(
      [1,1,2,3,3,3,"a","a",5,5,"e","e"]
    ) == [1,2,3,"a",5,"e"]
  end
end
