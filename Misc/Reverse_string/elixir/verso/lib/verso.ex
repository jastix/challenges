defmodule Verso do
  @moduledoc """
    Several ways to reverse a string.
  """
  def reverse_with_list(str), do: rev(String.codepoints(str))

  defp rev([h]),   do: h
  defp rev([h|t]), do: rev(t) <> h

  def reverse_with_slice(""), do: ""
  def reverse_with_slice(str) do
    String.slice(str, String.length(str) - 1, "") <>
    reverse_with_slice(String.slice(str, 0, String.length(str) - 1))
  end

  def reverse_with_foldr(str) do
    str
    |> String.codepoints
    |> List.foldr("", fn(el, acc) -> acc <> el end)
  end

  def reverse_with_next(str), do: rev_n(String.next_grapheme(str))

  defp rev_n({h, ""}), do: h
  defp rev_n({h, t}),  do: rev_n(String.next_grapheme(t)) <> h

  def reverse_reverse(str), do: String.reverse(str)
end
