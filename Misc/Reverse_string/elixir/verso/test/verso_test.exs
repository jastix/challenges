defmodule VersoTest do
  use ExUnit.Case
  doctest Verso

  test "reverse with list" do
    string = "abcd1234"
    assert Verso.reverse_with_list(string) == "4321dcba"
  end

  test "reverse with slice" do
    string = "abcd1234"
    assert Verso.reverse_with_slice(string) == "4321dcba"
  end

  test "reverse with foldr (string)" do
    string = "abcd1234"
    assert Verso.reverse_with_foldr(string) == "4321dcba"
  end

  test "reverse with next" do
    string = "abcd1234"
    assert Verso.reverse_with_next(string) == "4321dcba"
  end

  test "String.reverse" do
    string = "abcd1234"
    assert Verso.reverse_reverse(string) == "4321dcba"
  end
end
