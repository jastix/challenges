defmodule Reverse do
  use Benchfella

  bench "reverse with list" do
    Verso.reverse_with_list "This is just a long string to test my code!"
  end

  bench "reverse with slice" do
    Verso.reverse_with_slice "This is just a long string to test my code!"
  end

  bench "reverse with foldr" do
    Verso.reverse_with_foldr "This is just a long string to test my code!"
  end

  bench "String.reverse" do
    String.reverse "This is just a long string to test my code!"
  end
end
