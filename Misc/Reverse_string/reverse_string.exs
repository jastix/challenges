defmodule Str do
  def reverse(str), do: rev(String.codepoints(str))
  defp rev([h]), do: h
  defp rev([h|t]), do: rev(t) <> h
end
