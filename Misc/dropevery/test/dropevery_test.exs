defmodule DropeveryTest do
  use ExUnit.Case
  doctest Dropevery

  test "returns empty list when empty list is given" do
    assert Dropevery.drop_every([], 3) == []
  end

  test "drop elements from a list" do
    assert Dropevery.drop_every([1,1,2,2], 2) == [1, 2]
  end

  test "returns the same list if the list is shorter than count" do
    assert Dropevery.drop_every([1,2,3], 4) == [1,2,3]
  end
end
