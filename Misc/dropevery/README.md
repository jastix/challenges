# Dropevery

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add dropevery to your list of dependencies in `mix.exs`:

        def deps do
          [{:dropevery, "~> 0.0.1"}]
        end

  2. Ensure dropevery is started before your application:

        def application do
          [applications: [:dropevery]]
        end

