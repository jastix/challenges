defmodule Dropevery do
  @doc """
  Ninety-Nine Problems
  P16 (**) Drop every N'th element from a list

  example:
  iex> Dropevery.drop_every(["a","b","c","d","e","f","g","h","i","j","k"], 3)
  ["a", "b", "d", "e", "g", "h", "j", "k"]
  """
  def drop_every([], _n), do: []
  def drop_every([h | t], count), do: _drop_every([h | t], count, count - 1)

  defp _drop_every([],      _count, _n), do: []
  defp _drop_every([_h | t], count,  0), do: _drop_every(t, count, count - 1)
  defp _drop_every([h | t] , count,  n), do: [h] ++ _drop_every(t, count, n - 1)
end
