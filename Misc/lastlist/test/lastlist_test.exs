defmodule LastlistTest do
  use ExUnit.Case
  doctest Lastlist

  test "returns last element of a list" do
    assert Lastlist.last([1,2,3]) == 3
  end

  test "empty list" do
    assert Lastlist.last([]) == []
  end

  test "one element list" do
    assert Lastlist.last([1]) == 1
  end
end
