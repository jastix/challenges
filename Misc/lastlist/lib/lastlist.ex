defmodule Lastlist do
  @moduledoc """
  Ninety-Nine Problems
  P01 (*) Find the last element of a list

  example:
  iex> Lastlist.last([1,2,3])
  3
  """

  def last([]),       do: []
  def last([h | []]), do: h
  def last([_ | t]),  do: last(t)
end
