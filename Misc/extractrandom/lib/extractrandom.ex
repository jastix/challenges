defmodule Extractrandom do
  @doc """
  Ninety-Nine Problems
  P23 (**) Extract a given number of randomly selected elements from a list

  example:
  iex> Extractrandom.rnd_select(["a","b","c","d","e","f","g","h"], 3)
  ["e","d","a"]
  """

  def rnd_select([], _n), do: []
  def rnd_select(list, n) do
    _rnd_select(list, Enum.take_random(0..(length(list)-1), n), 0)
  end

  defp _rnd_select([], _elements, _count), do: []
  defp _rnd_select([h | t], elements, count) do
    case Enum.member?(elements, count) do
      true ->
        [h] ++ _rnd_select(t, elements, count + 1)
      _ ->
        _rnd_select(t, elements, count + 1)
    end
  end
end
