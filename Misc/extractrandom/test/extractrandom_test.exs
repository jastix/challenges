defmodule ExtractrandomTest do
  use ExUnit.Case

  test "returns empty list when given an empty list" do
    assert Extractrandom.rnd_select([], 3) == []
  end

  test "it extracts one element from a list" do
    :rand.seed(:exsplus, {1,2,3})
    assert Extractrandom.rnd_select([1,2,3], 1) == [2]
  end

  test "it extract a given number of elements from a list" do
    :rand.seed(:exsplus, {3,4,4})
    assert Extractrandom.rnd_select(["a","b","c","d","e","f","g","h"], 3) == ["c","f","g"]
  end
end
