defmodule ListsliceTest do
  use ExUnit.Case
  doctest Listslice

  test "it returns an empty list when empty list is given" do
    assert Listslice.slice([], 3, 7) == []
  end

  test "it extracts a slice from an integer list" do
    assert Listslice.slice([1,2,3,4,5], 1, 3) == [1,2,3]
  end

  test "it extracts a slice from a list when finish < start" do
    assert Listslice.slice(["a","b","c","d","e"], 5, 2)  == ["b","c","d","e"]
  end
end
