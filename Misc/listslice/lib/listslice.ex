defmodule Listslice do
  @doc """
  Ninety-Nine Problems
  P18 (**) Extract a slice from a list
  Given two indices, I and K, the slice is the list
  containing the elements between the I'th and K'th element
  of the original list (both limits included).
  Start counting the elements with 1.

  example:
  iex> Listslice.slice(["a","b","c","d","e","f","g","h","i","k"], 3, 7)
  ["c","d","e","f","g"]
  """

  def slice([], _start, _finish), do: []
  def slice(list, start, finish), do: _slice(list, start, finish, 1)

  defp _slice([h | t], start, finish, count) when count in start..finish do
    [h] ++ _slice(t, start, finish, count + 1)
  end

  defp _slice([_h | t], start, finish, count) do
    _slice(t, start, finish, count + 1)
  end

  defp _slice([], _start, _finish, _count), do: []
end
