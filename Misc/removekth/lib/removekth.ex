defmodule Removekth do
  @doc """
  Ninety-Nine Problems
  P20 (*) Remove the K'th element from a list

  example:
  iex> Removekth.remove_at(["a","b","c","d"], 2)
  {["a","c","d"], "b"}
  """

  def remove_at([], _n), do: []
  def remove_at(list, n) when length(list) < n, do: list
  def remove_at(list, n), do: _remove_at(list, n, [])

  defp _remove_at([h | t], 1, sublist), do: {sublist ++ t, h}
  defp _remove_at([h | t], n, sublist) do
    _remove_at(t, n - 1, [h] ++ sublist)
  end
end
