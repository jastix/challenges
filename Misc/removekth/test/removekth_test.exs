defmodule RemovekthTest do
  use ExUnit.Case
  doctest Removekth

  test "it returns empty list when empty list is given" do
    assert Removekth.remove_at([], 3) == []
  end

  test "it removes element from a list" do
    assert Removekth.remove_at([1,2,3], 1) == {[2,3], 1}
  end

  test "it removes element from a list when K > length of the list" do
    assert Removekth.remove_at([1,2,3,4], 5) == [1,2,3,4]
  end
end
