defmodule Listrange do
  @doc """
  Ninety-Nine Problems
  P22 (*) Create a list containing all integers within a given range

  example:
  iex> Listrange.range(4, 9)
  [4,5,6,7,8,9]
  """

  def range(finish, finish), do: [finish]
  def range(start, finish),  do: [start] ++ range(start + 1, finish)
end
