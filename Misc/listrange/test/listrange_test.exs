defmodule ListrangeTest do
  use ExUnit.Case
  doctest Listrange

  test "it returns one element range if the provided range is 1" do
    assert Listrange.range(1,1) == [1]
  end

  test "it returns a list with integers" do
    assert Listrange.range(1, 5) == [1,2,3,4,5]
  end
end
