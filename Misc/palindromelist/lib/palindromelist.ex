defmodule Palindromelist do
  @moduledoc """
  Ninety-Nine Problems
  P06 (*) Find out whether a list is a palindrome

  example:
  iex> Palindromelist.is_palindrome?(["x","a","m","a","x"])
  true
  """

  def is_palindrome?([]),        do: true
  def is_palindrome?([_h | []]), do: true

  def is_palindrome?(list), do: _is_palindrome?(list, _reverse(list))

  defp _is_palindrome?([], []),             do: true
  defp _is_palindrome?([h|t], [h|ts]),      do: _is_palindrome?(t, ts)
  defp _is_palindrome?([_h|_t], [_hs|_ts]), do: false

  defp _reverse([]),    do: []
  defp _reverse([h|t]), do: _reverse(t) ++ [h]
end
