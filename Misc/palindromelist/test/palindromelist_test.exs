defmodule PalindromelistTest do
  use ExUnit.Case
  doctest Palindromelist

  test "returns true for empty list" do
    assert Palindromelist.is_palindrome?([]) == true
  end

  test "returns true for one element list" do
    assert Palindromelist.is_palindrome?([1]) == true
  end

  test "returns true for palindrome list" do
    assert Palindromelist.is_palindrome?([1,2,3,2,1]) == true
  end

  test "returns false for non palindrome list" do
    assert Palindromelist.is_palindrome?([1,2,3,4]) == false
  end

  test "returns true for list with letters" do
    assert Palindromelist.is_palindrome?(["a", "b", "c", "b", "a"]) == true
  end
end
