defmodule Listcount do
  @moduledoc """
  Ninety-Nine Problems
  P04 (*) Find the number of elements of a list

  example:
  iex> Listcount.length([1,2,3,4])
  4
  """

  def length(list), do: _length(list, 0)

  defp _length([], acc),       do: acc
  defp _length([_h | t], acc), do: _length(t, acc + 1)
end
