# Listcount

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add listcount to your list of dependencies in `mix.exs`:

        def deps do
          [{:listcount, "~> 0.0.1"}]
        end

  2. Ensure listcount is started before your application:

        def application do
          [applications: [:listcount]]
        end

