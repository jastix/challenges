defmodule ListcountTest do
  use ExUnit.Case
  doctest Listcount

  test "it returns 0 for empty list" do
    assert Listcount.length([]) == 0
  end

  test "it returns length of the list" do
    assert Listcount.length([1,2,3]) == 3
  end
end
