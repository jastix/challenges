defmodule Kthlist do
  @moduledoc """
  Ninety-Nine Problems
  P03 (*) Find the K'th element of a list

  example:
  iex> Kthlist.find_element([1,2,3], 2)
  3
  """

  def find_element([], _),      do: []
  def find_element([h | _], 0), do: h
  def find_element([_ | t], position) do
    find_element(t, position - 1)
  end
end
