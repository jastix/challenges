# Kthlist

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add kthlist to your list of dependencies in `mix.exs`:

        def deps do
          [{:kthlist, "~> 0.0.1"}]
        end

  2. Ensure kthlist is started before your application:

        def application do
          [applications: [:kthlist]]
        end

