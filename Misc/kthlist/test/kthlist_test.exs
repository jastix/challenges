defmodule KthlistTest do
  use ExUnit.Case
  doctest Kthlist

  test "finds second element of a list" do
    assert Kthlist.find_element([1,2,3], 1) == 2
  end

  test "returns empty list if the list is empty" do
    assert Kthlist.find_element([], 3) == []
  end

  test "returns empty list if position is out of bounds of the list" do
    assert Kthlist.find_element([1,2], 3) == []
  end
end
