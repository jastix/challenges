defmodule ListinsertelTest do
  use ExUnit.Case
  doctest Listinsertel

  test "it returns list with provided element when given list is empty" do
    assert Listinsertel.insert_at([], "a", 2) == ["a"]
  end

  test "it adds new element to the head of a list" do
    assert Listinsertel.insert_at([2,3], 1, 1) == [1,2,3]
  end

  test "it adds a new element into the list" do
    assert Listinsertel.insert_at(["a","b","c","d"], "x", 3) == ["a","b","x","c","d"]
  end

  test "it adds new element to the end of the list if the list is shorter than given position" do
    assert Listinsertel.insert_at(["a","b"], "d", 4) == ["a","b","d"]
  end
end
