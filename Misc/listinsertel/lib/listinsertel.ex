defmodule Listinsertel do
  @doc """
  Ninety-Nine Problems
  P21 (*) Insert an element at a given position into a list

  example:
  iex> Listinsertel.insert_at(["a","b","c","d"], "alfa", 2)
  ["a","alfa","b","c","d"]
  """

  def insert_at([], el, _n), do: [el]
  def insert_at([h | t], el, 1), do: [el] ++ [h | t]
  def insert_at([h | t], el, n), do: [h] ++ insert_at(t, el, n - 1)
end
