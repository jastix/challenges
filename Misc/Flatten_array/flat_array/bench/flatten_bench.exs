defmodule FlattenBench do
  use Benchfella

  @list [1, [2], [3, [4, [5]]], 6, [7], 8, [9, [10, [11, [12]]]]]

  bench "pattern matching flatten" do
    FlatArray.flatten @list
  end

  bench "native flatten" do
    List.flatten @list
  end
end
