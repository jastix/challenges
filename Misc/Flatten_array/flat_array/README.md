# FlatArray

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add flat_array to your list of dependencies in `mix.exs`:

        def deps do
          [{:flat_array, "~> 0.0.1"}]
        end

  2. Ensure flat_array is started before your application:

        def application do
          [applications: [:flat_array]]
        end

