defmodule FlatArrayTest do
  use ExUnit.Case
  doctest FlatArray

  test "it flattens one element list" do
    assert FlatArray.flatten([[[2]]]) == [2]
  end

  test "it flattens a list with one deeply nested element" do
    assert FlatArray.flatten([1, [2]]) == [1, 2]
  end

  test "it flattens the list" do
    assert FlatArray.flatten([1,[2],3,[4, [5]]]) == [1,2,3,4,5]
  end
end
