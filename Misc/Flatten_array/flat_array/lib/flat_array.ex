defmodule FlatArray do
  @moduledoc """
  Flattens arbitrarily nested list.
  """

  def flatten(list), do: _flatten(list, [])

  defp _flatten([], acc),          do: acc
  defp _flatten([h | []], acc),    do: _flatten(h, acc)
  defp _flatten([[h] | t], acc),   do: _flatten([h | t], acc)
  defp _flatten([h | [ t ]], acc), do: _flatten([h | t], acc)
  defp _flatten([h | t], acc),     do: [h | _flatten(t, acc)]
  defp _flatten(el, acc),          do: [el | acc]
end
