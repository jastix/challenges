defmodule Packduplicates do
  @moduledoc """
  Ninety-Nine Problems
  P09 (**) Pack consecutive duplicates of list elements into sublists

  example:
  iex> Packduplicates.pack(["a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e"])
  [["a","a","a","a"],["b"],["c","c"],["a","a"],["d"],["e","e","e","e"]]
  """

  def pack([]), do: []
  def pack(list), do: _pack(list, [])

  defp _pack([], sublist       ), do: [ sublist ]
  defp _pack([h | t], []       ), do: _pack(t, [h])
  defp _pack([h | t], [h | t]  ), do: _pack(t, [h] ++ [h | t])
  defp _pack([h | t], [h | th] ), do: _pack(t, [h] ++ [h | th])
  defp _pack([h | t], [ht | th]), do: [[ht | th]] ++ _pack(t, [h])
end
