defmodule PackduplicatesTest do
  use ExUnit.Case
  doctest Packduplicates

  test "it returns empty list if empty list is given" do
    assert Packduplicates.pack([]) == []
  end

  test "it packs 3 consecutive elements into list" do
    assert Packduplicates.pack([1,1,1]) == [[1,1,1]]
  end
  test "it packs consecutive elements into lists" do
    assert Packduplicates.pack(["a", "a", "b", "c", "c"]) == [["a", "a"], ["b"], ["c", "c"]]
  end
end
