defmodule SecondLast do
  @moduledoc """
  Ninety-Nine Problems
  P02 (*) Find the last but one element of a list

  example:
  iex> SecondLast.second_last([1,2,3])
  2
  """

  def second_last([]) do
    raise ArgumentError, message: "list is empty"
  end

  def second_last([_h | []]) do
    raise ArgumentError, message: "Can't find second to last element from a list with less than 2 elements"
  end

  def second_last([h | [_t| []]]), do: h
  def second_last([_h | t]),       do: second_last(t)
end
