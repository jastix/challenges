defmodule SecondLastTest do
  use ExUnit.Case
  doctest SecondLast

  test "returns second to last element of a list" do
    assert SecondLast.second_last([1,2,3]) == 2
  end

  test "raises an error if the list is empty" do
    assert_raise ArgumentError, "list is empty", fn ->
      SecondLast.second_last([])
    end
  end

  test "raises an error if the list has only one element" do
    assert_raise ArgumentError, "Can't find second to last element from a list with less than 2 elements", fn ->
      SecondLast.second_last([1])
    end
  end
end
