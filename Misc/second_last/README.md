# SecondLast

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add second_last to your list of dependencies in `mix.exs`:

        def deps do
          [{:second_last, "~> 0.0.1"}]
        end

  2. Ensure second_last is started before your application:

        def application do
          [applications: [:second_last]]
        end

