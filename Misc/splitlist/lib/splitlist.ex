defmodule Splitlist do
  @doc """
  Ninety-Nine Problems
  P17 (*) Split a list into two parts;
  the length of the first part is given

  example:
  iex> Splitlist.split(["a","b","c","d","e","f","g","h","i","k"], 3)
  {["a","b","c"], ["d","e","f","g","h","i","k"]}
  """

  def split([], _n), do: []
  def split(list, count), do: _split(list, count, [])

  defp _split([],  _n, sublist), do: {Enum.reverse(sublist), []}
  defp _split(list, 0, sublist), do: {Enum.reverse(sublist), list}
  defp _split([h | t], count, sublist), do: _split(t, count - 1, [h] ++ sublist)
end
