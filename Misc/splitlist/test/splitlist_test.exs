defmodule SplitlistTest do
  use ExUnit.Case
  doctest Splitlist

  test "it returns empty list when empty list is given" do
    assert Splitlist.split([], 3) == []
  end

  test "it splits given list" do
    assert Splitlist.split([1,2,3,4,5], 2) == {[1,2], [3,4,5]}
  end

  test "it returns the same list if the list is smaller than length" do
    assert Splitlist.split(["a","b","c"], 5) == {["a","b","c"], []}
  end
end
