# Splitlist

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add splitlist to your list of dependencies in `mix.exs`:

        def deps do
          [{:splitlist, "~> 0.0.1"}]
        end

  2. Ensure splitlist is started before your application:

        def application do
          [applications: [:splitlist]]
        end

