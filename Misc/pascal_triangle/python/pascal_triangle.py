from typing import List

"""Code to compute and pretty print Pascal's triangle
How to run:
$ python3 pascal_triangle.py
"""


def compute_triangle(size: int) -> List[List[int]]:
    """Computes rows of Pascal's triangle
    """
    triangle = []
    for row_num in range(size):
        if row_num == 0:
            triangle.append([1])
        elif row_num == 1:
            triangle.append([1, 1])
        else:
            prev_row = triangle[row_num-1]
            new_row = compute_row(prev_row, row_size=row_num)
            triangle.append(new_row)
    return triangle


def compute_row(prev_row: List[int], row_size: int) -> List[int]:
    """Function to build rows that require computation
    """
    curr_row = []
    for idx in range(row_size + 1):
        if (idx == 0) or (idx == row_size):
            curr_row.append(1)
        else:
            val = prev_row[idx-1] + prev_row[idx]
            curr_row.append(val)
    return curr_row


def print_triangle(triangle):
    """Prints triangle neatly with padding.
    Relies on str.center method that returns a string with padding
    https://docs.python.org/3.9/library/stdtypes.html?highlight=center#str.center
    """
    max_row_size = len(' '.join(map(str, triangle[-1])))
    for row in triangle:
        vals = ' '.join(map(str, row))
        print(vals.center(max_row_size))


if __name__ == '__main__':
    tr = compute_triangle(size=10)
    print_triangle(tr)
