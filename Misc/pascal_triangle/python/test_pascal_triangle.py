from pascal_triangle import compute_triangle


def test_one_element():
    assert compute_triangle(size=1) == [[1]]


def test_two_element():
    assert compute_triangle(size=2) == [[1], [1, 1]]


def test_three_element():
    assert compute_triangle(size=3) == [[1], [1, 1], [1, 2, 1]]


def test_five_element():
    result = [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1]]
    assert compute_triangle(size=5) == result
