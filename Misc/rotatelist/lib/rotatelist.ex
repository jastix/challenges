defmodule Rotatelist do
  @doc """
  Ninety-Nine Problems
  P19 (**) Rotate a list N places to the left

  examples:
  iex> Rotatelist.rotate(["a","b","c","d","e","f","g","h"], 3)
  ["d","e","f","g","h","a","b","c"]

  iex> Rotatelist.rotate(["a","b","c","d","e","f","g","h"], -2)
  ["g","h","a","b","c","d","e","f"]
  """

  def rotate([], _n), do: []
  def rotate(list, places), do: _rotate(list, places, length(list))

  defp _rotate(list, places, list_length) when places < 0 and abs(places) > list_length do
    _rotate(list, places + list_length, list_length)
  end

  defp _rotate(list, places, _list_length) when places < 0 do
    {first, second} = split(Enum.reverse(list), abs(places))
    Enum.reverse(first) ++ Enum.reverse(second)
  end

  defp _rotate(list, places, list_length) when places < list_length do
    {first, second} = split(list, places)
    second ++ first
  end

  defp _rotate(list, places, list_length) do
    _rotate(list, places - list_length, list_length)
  end

  def split([], _n), do: []
  def split(list, count), do: _split(list, count, [])

  defp _split([],  _n, sublist), do: {Enum.reverse(sublist), []}
  defp _split(list, 0, sublist), do: {Enum.reverse(sublist), list}
  defp _split([h | t], count, sublist), do: _split(t, count - 1, [h] ++ sublist)
end
