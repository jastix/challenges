defmodule RotatelistTest do
  use ExUnit.Case
  doctest Rotatelist

  test "it returns empty list when empty list is given" do
    assert Rotatelist.rotate([], 3) == []
  end

  test "it rotates a list" do
    assert Rotatelist.rotate([1,2,3], 1) == [2,3,1]
  end

  test "it rotates a list when number of places is higher than length of the list" do
    assert Rotatelist.rotate(["a","b","c"], 5) == ["c", "a", "b"]
  end

  test "it rotates a list with negative number of places" do
    assert Rotatelist.rotate([1,2,3], -2) == [2,3,1]
  end
  test "it rotates a list with negative number of places that is higher than length of the list" do
    assert Rotatelist.rotate(["a","b","c"], -5) == ["b","c","a"]
  end
end
