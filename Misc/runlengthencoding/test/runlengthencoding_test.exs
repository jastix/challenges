defmodule RunlengthencodingTest do
  use ExUnit.Case
  doctest Runlengthencoding

  test "it returns list with an empty list when given an empty list" do
    assert Runlengthencoding.encode([]) == []
  end

  test "it performs run-length encoding on a simple list" do
    assert Runlengthencoding.encode([1,1,1]) == [{3, 1}]
  end

  test "it performs run-length encoding" do
    assert Runlengthencoding.encode(["a", "a", "a", "b", "b", 2, "c", "c", "3", "3", "d"]) ==
      [{3, "a"}, {2, "b"}, {1, 2}, {2, "c"}, {2, "3"}, {1, "d"}]
  end

  ### encode_modified
  test "it returns empty list when given an empty list" do
    assert Runlengthencoding.encode_modified([]) == []
  end

  test "it returns one element list when given one element list" do
    assert Runlengthencoding.encode_modified([1]) == [1]
  end

  test "it performs modified run length encoding" do
    assert Runlengthencoding.encode_modified(["a", "a", "b", "b", "b", 2, 5, 5, "c"]) == [{2, "a"}, {3, "b"}, 2, {2, 5}, "c"]
  end

  ### decode
  test "returns empty list when empty list is given" do
    assert Runlengthencoding.decode([]) == []
  end

  test "decodes a one element list" do
    assert Runlengthencoding.decode(["a"]) == ["a"]
  end

  test "decodes a list" do
    assert Runlengthencoding.decode([{2, "a"}, "b", {3, "c"}, {2, 1}, "d", "e"])
  end

  ### encode direct
  test "it returns empty list when empty list is given" do
    assert Runlengthencoding.encode_direct([]) == []
  end

  test "it encodes directly a simple list" do
    assert Runlengthencoding.encode_direct(["a", "a", "a"]) == [{3, "a"}]
  end

  test "it encodes directly a list" do
    assert Runlengthencoding.encode_direct(
      ["a", "a", "a", "a", "b", "c", "c", "a", "a", "d"]) ==
      [{4, "a"}, {1, "b"}, {2, "c"}, {2, "a"}, {1, "d"}]
    
  end
end
