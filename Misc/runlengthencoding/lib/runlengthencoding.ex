defmodule Runlengthencoding do
  @doc """
  Ninety-Nine Problems
  P10 (*) Run-length encoding of a list
  Use the result of problem P09 to implement the so-called
  run-length encoding data compression method. Consecutive
  duplicates of elements are encoded as terms [N,E] where N
  is the number of duplicates of the element E.

  example:
  iex> Runlengthencoding.encode(["a","a","a","a","b","c","c","a","a","d","e","e","e","e"])
  [{4, "a"}, {1, "b"}, {2, "c"}, {2, "a"}, {1, "d"}, {4, "e"}]
  """

  def encode([]),   do: []
  def encode(list), do: _encode(list, {})

  defp _encode([], sublist     ), do: [ sublist ]
  defp _encode([h | t], {}     ), do: _encode(t, {1, h})
  defp _encode([h | t], {n, h} ), do: _encode(t, {n + 1, h})
  defp _encode([h | t], {n, ht}), do: [{n, ht}] ++ _encode(t, {1, h})

  @doc """
  Ninety-Nine Problems
  P11 (*) Modified run-length encoding
  Modify the result of problem P10 in such a way
  that if an element has no duplicates it is
  simply copied into the result list. Only elements
  with duplicates are transferred as [N,E] terms.

  example:
  iex> Runlengthencoding.encode_modified([1,1,"a","b","b","b"])
  [{2, 1}, "a", {3, "b"}]
  """

  def encode_modified([]), do: []
  def encode_modified(list), do: _encode_modified(list, {})

  defp _encode_modified([], {1, h}      ), do: [ h ]
  defp _encode_modified([], sublist     ), do: [ sublist ]
  defp _encode_modified([h | t], {}     ), do: _encode_modified(t, {1, h})
  defp _encode_modified([h | t], {n, h} ), do: _encode_modified(t, {n + 1, h})
  defp _encode_modified([h | t], {1, ht}), do: [ht] ++ _encode_modified(t, {1, h})
  defp _encode_modified([h | t], {n, ht}), do: [{n, ht}] ++ _encode_modified(t, {1, h})


  @doc """
  Ninety-Nine Problems
  P12 (*) Decode a run-length encoded list

  example:
  iex> Runlengthencoding.decode([{4, "a"}, "b", {2, "c"}])
  ["a", "a", "a", "a", "b", "c", "c"]
  """

  def decode([]), do: []
  def decode([{n, h} | t]), do: List.duplicate(h, n) ++ decode(t)
  def decode([h | t]     ), do: [ h ] ++ decode(t)

  @doc """
  Ninety-Nine Problems
  P13 (**) Run-length encoding of a list (direct solution)

  Implement the so-called run-length encoding data compression method
  directly. i.e. don't explicitly create the sublists containing
  the duplicates, as in problem P09, but only count them.
  """

  def encode_direct([]), do: []
  def encode_direct([h | t]), do: _encode_direct([h | t], 0)

  defp _encode_direct([], _count             ), do: []
  defp _encode_direct([h | [ h | t ]], count ), do: _encode_direct([h | t], count + 1)
  defp _encode_direct([h | [ ht| t ]], count ), do: [{count + 1, h}] ++ _encode_direct([ht | t], 0)
  defp _encode_direct([h | []], count        ), do: [{count + 1, h}]
end
