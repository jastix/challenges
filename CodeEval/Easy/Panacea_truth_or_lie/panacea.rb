#*Input:*
#The first argument is a path to a file. Each line includes a test case with virus
#components in the hexadecimal numeral system (HEX) and antivirus components
#in the binary number system (BIN). Virus and antivirus components are separated
#by a pipeline '|'.
#Input example is the following
# ========
#64 6e 78 | 100101100 11110
#5e 7d 59 | 1101100 10010101 1100111
#93 75 | 1000111 1011010 1100010
# ========
#
#*Output*
#Your task is to calculate the sum of all virus components and compare it
#with the sum of antivirus components. If the numbers are the same or the
#sum of antivirus components is greater than the sum of virus components, this
#means that the virus was stopped. So, print True. Otherwise, print False.

def virus_stopped?(virus, antivirus)
  antivirus >= virus ? 'True' : 'False'
end

def binary_to_int(bin)
  bin.to_i(2)
end

File.open(ARGV[0]).each_line do |line|
  unless line.chomp.empty?
    virus_components, antivirus_components = line.chomp.split(' | ')
    virus = virus_components.split(' ').map(&:hex).reduce(:+)
    antivirus = antivirus_components.split(' ').map { |comp| binary_to_int(comp) }
                .reduce(:+)
    puts virus_stopped?(virus, antivirus)
  end
end
