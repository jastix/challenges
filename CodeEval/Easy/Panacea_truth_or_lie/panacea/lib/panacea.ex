defmodule Panacea do
  @moduledoc """
  Checks if virus score (in hex) is less than antivirus score (in binary).
  """
  def main(argv) do
    parse_args(argv)
  end

  def parse_args(" "), do: []
  def parse_args(arg) do
    case File.exists?(arg) do
      true ->
        arg
        |> File.stream!
        |> Stream.map(fn(line) -> compute(line) end)
        |> Enum.each(fn(result) -> format_output(result) end)
      _ ->
        IO.puts "File not found"
    end
  end

  def compute(data) do
    [virus, antivirus] = String.strip(data)
      |> String.split(" | ", trim: true)
    {virus_score(virus), antivirus_score(antivirus)}
    |> is_virus_stopped?

  end

  def is_virus_stopped?({virus_score, antivirus_score}) do
    antivirus_score >= virus_score
  end

  defp virus_score(virus) do
    virus
    |> String.split(" ", trim: true)
    |> Stream.map(fn(comp) -> String.to_integer(comp, 16) end)
    |> Enum.sum
  end

  defp antivirus_score(antivirus) do
    antivirus
    |> String.split(" ", trim: true)
    |> Stream.map(fn(comp) -> String.to_integer(comp, 2) end)
    |> Enum.sum
  end

  def format_output(true),  do: IO.puts "True"
  def format_output(false), do: IO.puts "False"
end

# test will fail here
System.argv |> Panacea.main
