defmodule PanaceaTest do
  use ExUnit.Case
  doctest Panacea

  test "antivirus wins" do
    assert Panacea.compute("64 6e 78 | 100101100 11110") == true
  end

  test "virus wins" do
    assert Panacea.compute("93 75 | 1000111 1011010 1100010") == false
  end
end
