defmodule Prime do

  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer
  def nth(count) do
    case count < 1 do
      true -> raise ArgumentError
      false -> find_nth_prime(count)
    end
  end

  defp find_nth_prime(count) do
    Stream.iterate(2, &next_prime/1)
    |> Enum.take(count)
    |> List.last
  end

  defp next_prime(num) do
    num = num + 1
    case is_prime?(num) do
      true  -> num
      false -> next_prime(num)
    end
  end

  # from SICP
  defp is_prime?(num) do
    num == smallest_divisor(num)
  end

  defp smallest_divisor(num) do
    find_divisor(num, 2)
  end

  defp find_divisor(num, test_divisor) do
    cond do
      :math.sqrt(test_divisor) > num -> num 
      divides?(test_divisor, num) -> test_divisor
      true -> find_divisor(num, test_divisor + 1) 
    end
  end

  defp divides?(a, b), do: rem(b, a) == 0
end
