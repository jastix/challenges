defmodule SpaceAge do
  @type planet :: :mercury | :venus | :earth | :mars | :jupiter
                | :saturn | :neptune | :uranus

  @doc """
  Return the number of years a person that has lived for 'seconds' seconds is
  aged on 'planet'.
  """
  @spec age_on(planet, pos_integer) :: float
  def age_on(planet, seconds) do
    earth_years = seconds / 31557600
    _age_on(planet, seconds, earth_years)
  end

  defp _age_on(:earth,   _seconds, earth_years), do: earth_years
  defp _age_on(:mercury, seconds, earth_years),  do: earth_years / 0.2408467
  defp _age_on(:venus,   seconds, earth_years),  do: earth_years / 0.61519726
  defp _age_on(:mars,    seconds, earth_years),  do: earth_years / 1.8808158
  defp _age_on(:jupiter, seconds, earth_years),  do: earth_years / 11.862615
  defp _age_on(:saturn,  seconds, earth_years),  do: earth_years / 29.447498
  defp _age_on(:uranus,  seconds, earth_years),  do: earth_years / 84.016846
  defp _age_on(:neptune, seconds, earth_years),  do: earth_years / 164.79132
end
