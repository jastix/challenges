defmodule BracketPush do
  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @opening_brackets ["(", "{", "["]
  @closing_brackets [")", "}", "]"]
  @brackets Enum.zip(@opening_brackets, @closing_brackets) |> Enum.into(%{})

  @spec check_brackets(String.t) :: boolean
  def check_brackets(str) do
    str
    |> String.codepoints
    |> Enum.filter(&(&1 in @opening_brackets ++ @closing_brackets))
    |> _check_brackets
  end

  defp _check_brackets(str, seen \\ [])
  defp _check_brackets([], []),        do: true
  defp _check_brackets([], [_h | _t]), do: false
  defp _check_brackets([h | t], seen) when h in @opening_brackets do
    _check_brackets(t, [h] ++ seen)
  end
  defp _check_brackets([h | _t], []) when h in @closing_brackets, do: false
  defp _check_brackets([h | t], [hs | ts]) when h in @closing_brackets do
    case h == @brackets[hs] do
      true -> _check_brackets(t, ts)
      false -> false
    end
  end
end
