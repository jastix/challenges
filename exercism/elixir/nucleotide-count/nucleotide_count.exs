defmodule DNA do
  @nucleotides [?A, ?C, ?G, ?T]

  @doc """
  Counts individual nucleotides in a DNA strand.

  ## Examples

  iex> DNA.count('AATAA', ?A)
  4

  iex> DNA.count('AATAA', ?T)
  1
  """
  @spec count([char], char) :: non_neg_integer
  def count(strand, nucleotide) do
    with true <- is_valid_nucleotide?(nucleotide),
         true <- is_valid_seq?(strand) do
      Enum.count(strand, &(&1 == nucleotide))
    else
      false ->
        raise ArgumentError
    end
  end

  @doc """
  Returns a summary of counts by nucleotide.

  ## Examples

  iex> DNA.histogram('AATAA')
  %{?A => 4, ?T => 1, ?C => 0, ?G => 0}
  """
  @spec histogram([char]) :: map
  def histogram(strand) do
    def_seq =
      @nucleotides
      |> Enum.reduce(%{}, fn(x, acc) -> Map.put(acc, x, 0) end)
    _histogram(strand, def_seq)
  end

  defp _histogram('', def_seq), do: def_seq
  defp _histogram(strand, def_seq) do
    case is_valid_seq?(strand) do
      true ->
        count_nucleotides(strand, def_seq)
      false ->
        raise ArgumentError
    end
  end

  defp is_valid_nucleotide?(nuc) do
    Enum.member?(@nucleotides, nuc)
  end

  defp is_valid_seq?(strand) do
    Enum.all?(strand, &(is_valid_nucleotide?(&1)))
  end

  defp count_nucleotides(strand, def_seq) do
     Enum.reduce(strand, def_seq, fn(x, acc) ->
       Map.update(acc, x, 0, &(&1 + 1))
     end)
  end
end
