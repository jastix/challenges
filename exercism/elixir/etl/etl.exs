defmodule ETL do
  @doc """
  Transform an index into an inverted index.

  ## Examples

  iex> ETL.transform(%{"a" => ["ABILITY", "AARDVARK"], "b" => ["BALLAST", "BEAUTY"]})
  %{"ability" => "a", "aardvark" => "a", "ballast" => "b", "beauty" =>"b"}
  """
  @spec transform(map) :: map
  def transform(input) do
    input
    |> convert_to_list_of_value_key_tuples
    |> convert_tuples_to_map
  end

  defp convert_to_list_of_value_key_tuples(input) do
    Enum.reduce(input, [], fn({key, values}, acc) ->
      acc ++ swap_values_and_keys(values, key)
    end)
  end

  defp swap_values_and_keys(values, key) do
    Enum.reduce(values, [], fn(value, acc) ->
      [{String.downcase(value), key}] ++ acc
    end)
  end

  defp convert_tuples_to_map(tuples) do
    Enum.reduce(tuples, %{}, fn(kv, acc) -> Enum.into([kv], acc) end)
  end

  # # someone's cool use of comprehensions
  # @spec transform(map) :: map
  # def transform(input) do
    # for {k, l} <- input, value <- l, into: %{} do
    #   {String.downcase(value), k}
    # end
  # end

  # # nested Enum.reduce
  # @spec transform(map) :: map
  # def transform(input) do
  #   Enum.reduce(input, %{}, fn({k, v}, acc) ->
  #     Enum.reduce(v, acc, fn(value, acc) -> Map.put(acc, String.downcase(value), k) end)
  #   end)
  # end
end

