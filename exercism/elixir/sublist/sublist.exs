defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, a), do: :equal
  def compare(a, b) do
    cond do
      _is_sublist?(a, b) ->
        :sublist
      _is_superlist?(a, b) ->
        :superlist
      true ->
        :unequal
    end
  end

  defp _is_sublist?([], _), do: true
  defp _is_sublist?(_, []), do: false
  defp _is_sublist?(a, a), do: true
  defp _is_sublist?(a, b) when length(b) > length(a) do
    case a === Enum.take(b, length(a)) do
      true -> true
      false ->
        _is_sublist?(a, Enum.drop(b, 1))
    end
  end
  defp _is_sublist?(_a, _b), do: false

  defp _is_superlist?(a, b), do: _is_sublist?(b, a)
end
