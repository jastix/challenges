defmodule Raindrops do
  @prime_factors %{3 => "Pling", 5 => "Plang", 7 => "Plong"}

  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """
  @spec convert(pos_integer) :: String.t
  def convert(number) do
    number
    |> find_prime_factors
    |> translate(number)
  end

  def find_prime_factors(number) do
    Enum.filter(@prime_factors, fn({k, v}) -> rem(number, k) == 0 end)
  end
  def translate([], number), do: number |> Integer.to_string
  def translate(factors, _number) do
    Enum.reduce(factors, "", fn({k, v}, acc) -> acc <> v end)
  end
end
