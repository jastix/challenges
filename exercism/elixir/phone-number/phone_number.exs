defmodule Phone do

  @valid_numbers ?0..?9
  @valid_characters [?(, ?), ?-, ?+, ?\s, ?.] ++ Enum.to_list(@valid_numbers)

  @doc """
  Remove formatting from a phone number.

  Returns "0000000000" if phone number is not valid
  (10 digits or "1" followed by 10 digits)

  ## Examples

  iex> Phone.number("123-456-7890")
  "1234567890"

  iex> Phone.number("+1 (303) 555-1212")
  "3035551212"

  iex> Phone.number("867.5309")
  "0000000000"
  """
  @spec number(String.t) :: String.t
  def number(raw) do
    raw
    |> String.to_charlist
    |> _number
  end

  defp _number(num) do
    digits_num = num |> Enum.filter(&(&1 in @valid_numbers))

    with true <- valid_characters?(num),
         true <- valid_length?(digits_num) do
      extract_number(digits_num)
    else
      false ->
        String.duplicate("0", 10)
    end
  end

  defp valid_characters?(num) do
    Enum.all?(num, &(&1 in @valid_characters))
  end

  defp valid_length?(num) do
    cond do
      length(num) == 11 and hd(num) == ?1 ->
        true
      length(num) == 10 ->
        true
      true ->
        false
    end
  end

  defp extract_number(num) when length(num) == 11 do
    num
    |> Enum.slice(1..Enum.count(num))
    |> extract_number
  end

  defp extract_number(num) do
    num
    |> Enum.filter(&(&1 in @valid_numbers))
    |> List.to_string
  end

  @doc """
  Extract the area code from a phone number

  Returns the first three digits from a phone number,
  ignoring long distance indicator

  ## Examples

  iex> Phone.area_code("123-456-7890")
  "123"

  iex> Phone.area_code("+1 (303) 555-1212")
  "303"

  iex> Phone.area_code("867.5309")
  "000"
  """
  @spec area_code(String.t) :: String.t
  def area_code(raw) do
    raw
    |> number
    |> extract_area
  end

  defp extract_area(num) when length(num) == 11 do
    num
    |> String.slice(1..String.length(num))
    |> extract_area
  end

  defp extract_area(num) do
    num
    |> String.slice(0, 3)
  end

  @doc """
  Pretty print a phone number

  Wraps the area code in parentheses and separates
  exchange and subscriber number with a dash.

  ## Examples

  iex> Phone.pretty("123-456-7890")
  "(123) 456-7890"

  iex> Phone.pretty("+1 (303) 555-1212")
  "(303) 555-1212"

  iex> Phone.pretty("867.5309")
  "(000) 000-0000"
  """
  @spec pretty(String.t) :: String.t
  def pretty(raw) do
    raw
    |> number
    |> _pretty
  end

  defp _pretty(num) do
    area = area_code(num)
    {exchange, subscriber} = num
      |> String.slice(3, String.length(num))
      |> String.split_at(3)

    "(#{area}) #{exchange}-#{subscriber}"
  end
end
