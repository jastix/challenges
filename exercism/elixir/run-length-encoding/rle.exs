defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "HORSE" => "1H1O1R1S1E"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "1H1O1R1S1E" => "HORSE"
  """
  @spec encode(String.t) :: String.t
  def encode(string) do
    string
    |> String.codepoints
    |> Enum.chunk_by(&(&1))
    |> Enum.map_join(fn(x) -> "#{Enum.count(x)}#{hd(x)}" end)
  end

  @spec decode(String.t) :: String.t
  def decode(string) do
    string
    |> split_numbers_and_letters
    |> Enum.reduce("", fn([num, letter], acc) ->
         acc <> String.duplicate(letter, String.to_integer(num))
       end)
  end

  def split_numbers_and_letters(string) do
    Regex.scan(~r/(\d+)(\D)/, string, capture: :all_but_first)
  end
end
