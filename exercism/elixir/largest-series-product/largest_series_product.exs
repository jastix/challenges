defmodule Series do

  @doc """
  Finds the largest product of a given number of consecutive numbers in a given string of numbers.
  """
  @spec largest_product(String.t, non_neg_integer) :: non_neg_integer
  def largest_product(_string, 0), do: 1

  @spec largest_product(String.t, non_neg_integer) :: non_neg_integer
  def largest_product(number_string, size) do
    cond do
      String.length(number_string) < size -> raise ArgumentError
      size < 0 -> raise ArgumentError
      true -> find_largest_product(number_string, size)
    end
  end

  defp find_largest_product(number_string, size) do
    number_string
    |> String.codepoints
    |> Enum.map(&String.to_integer/1)
    |> Enum.chunk(size, 1)
    |> Enum.map(fn(list) -> Enum.reduce(list, fn(el, acc) -> el * acc end) end)
    |> Enum.max
  end
end
