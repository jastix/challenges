defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t, [String.t]) :: [String.t]
  def match(base, candidates) do
    candidates
    |> remove_base(base)
    |> find_anagrams(base)
  end

  def remove_base(candidates, base) do
    Enum.filter(candidates, fn(x) ->
      String.downcase(x) !== String.downcase(base)
    end)
  end

  def find_anagrams(candidates, base) do
    sorted_base = base |> _sort_characters
    Enum.filter(candidates, fn(x) ->
      _sort_characters(x) === sorted_base
    end)
  end

  defp _sort_characters(word) do
    word
    |> String.downcase
    |> String.to_charlist
    |> Enum.sort
  end
end
