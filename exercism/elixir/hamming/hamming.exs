defmodule Hamming do
  @doc """
  Returns number of differences between two strands of DNA, known as the Hamming Distance.

  ## Examples

  iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
  {:ok, 4}
  """
  @spec hamming_distance([char], [char]) :: non_neg_integer
  def hamming_distance(strand1, strand2) do
    case length(strand1) == length(strand2) do
      true -> 
        _hamming_distance(strand1, strand2)
      false ->
        {:error, "Lists must be the same length"}
    end
  end

  defp _hamming_distance(strand1, strand2, count \\ 0)
  defp _hamming_distance([], [], count), do: {:ok, count}
  defp _hamming_distance([h | t1], [h | t2], count) do
    _hamming_distance(t1, t2, count)
  end
  defp _hamming_distance([_h1 | t1], [_h2 | t2], count) do
    _hamming_distance(t1, t2, count + 1)
  end
end
