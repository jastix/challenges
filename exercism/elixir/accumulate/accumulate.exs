defmodule Accumulate do
  @doc """
    Given a list and a function, apply the function to each list item and
    replace it with the function's return value.

    Returns a list.

    ## Examples

      iex> Accumulate.accumulate([], fn(x) -> x * 2 end)
      []

      iex> Accumulate.accumulate([1, 2, 3], fn(x) -> x * 2 end)
      [2, 4, 6]

  """

  @spec accumulate(list, (any -> any)) :: list
  def accumulate(list, fun) do
    _accumulate(list, fun)
  end

  defp _accumulate(list, fun, result \\ [])
  defp _accumulate([], _fun, result), do: reverse(result)
  defp _accumulate([h | t], fun, result) do
    _accumulate(t, fun, [fun.(h)] ++ result)
  end

  # function to reverse a list
  def reverse(list), do: _reverse(list)

  defp _reverse(list, result \\ [])
  defp _reverse([], result),      do: result
  defp _reverse([h | t], result), do: _reverse(t, [h] ++ result)
end
