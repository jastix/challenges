defmodule Isogram do
  @doc """
  Determines if a word or sentence is an isogram
  """
  @spec isogram?(String.t) :: boolean
  def isogram?(sentence) do
    sentence
    |> String.downcase
    |> String.codepoints
    |> Enum.filter(fn(x) -> String.match?(x, ~r/\w/) end)
    |> only_unique_letters?
  end

  defp only_unique_letters?(letters, seen \\ [])
  defp only_unique_letters?([], _seen), do: true
  defp only_unique_letters?([h | t], seen) do
    case h in seen do
      true -> false
      false -> only_unique_letters?(t, [h] ++ seen)
    end
  end
end
