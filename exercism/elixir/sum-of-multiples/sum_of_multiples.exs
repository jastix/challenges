defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the
  factors provided.
  """
  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(limit, factors) do
    1..(limit-1)
    |> _remove_non_multiples(factors)
    |> Enum.sum
  end

  defp _remove_non_multiples(number, factors) do
    Enum.filter(number, fn(num) ->
      Enum.any?(factors, fn(f) -> rem(num, f) == 0 end)
    end)
  end
end
