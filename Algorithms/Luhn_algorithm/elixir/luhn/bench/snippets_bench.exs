defmodule RemoveWhitespace do
	use Benchfella
  require Integer

  bench "Enum reject" do
    "6370 1675 9034 6211 774"
    |> String.codepoints
    |> Enum.reject(fn(x) -> x == " " end)
    |> List.to_string
  end

  bench "String.replace" do
    String.replace("6370 1675 9034 6211 774", " ", "")
  end

  bench "double every second with list" do
    6370167590346211774
    |> Integer.digits
    |> Stream.chunk(2, 2, [])
    |> Enum.reduce([], fn(x, acc) -> acc ++ double_second_element(x) end)
    |> List.flatten
  end

  bench "double every second with map" do
    6370167590346211774
    |> Integer.digits
    |> Enum.with_index
    |> Enum.map(&double_digit(&1))
  end

  defp double_second_element([a]),   do: [a]
  defp double_second_element([a,b]), do: [a, (b * 2)]

  defp double_digit({digit, index}) when Integer.is_odd(index) do
    digit * 2
  end
  defp double_digit({digit, _}), do: digit

end
