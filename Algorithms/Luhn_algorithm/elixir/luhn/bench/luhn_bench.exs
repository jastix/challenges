defmodule LuhnBench do
	use Benchfella

  bench "card number validity" do
    Luhn.check_validity "6370 1675 9034 6211 774"
  end
end
