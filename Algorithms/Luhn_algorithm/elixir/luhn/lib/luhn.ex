defmodule Luhn do
  require Integer

  @moduledoc """
  Implements Luhn algorithm for credit card number validation.

  ## Examples

      iex> Luhn.check_validity("1556 9144 6285 339")
      1
  """

  def main(argv) do
    parse_args(argv)
  end

  def parse_args(" "), do: []
  def parse_args(arg) do
    case File.exists?(arg) do
      true ->
        arg
        |> File.stream!
        |> Stream.map(fn(line) -> check_validity(line) end)
        |> Enum.each(fn(result) -> format_output(result) end)
      _ ->
        IO.puts "File not found"
    end
  end

  def check_validity(card_number) do
    card_number
    |> String.strip
    |> remove_whitespace
    |> String.reverse
    |> to_list_of_integers
    |> double_every_second_digit
    |> sum_double_numbers
    |> sum_digits
    |> modulo
    |> is_valid?
  end

  def remove_whitespace(string), do: String.replace(string, " ", "")

  def double_every_second_digit(number) do
    number
    |> Enum.with_index
    |> Enum.map(&double_digit(&1))
  end

  def sum_double_numbers(nums) do
    Enum.map(nums, fn(x) -> x |> Integer.digits |> sum_double_number end)
  end

  def sum_digits(list), do: Enum.sum(list)

  def format_output(1), do: IO.puts "Valid"
  def format_output(_), do: IO.puts "Invalid"

  def is_valid?(0), do: 1
  def is_valid?(_), do: 0

  defp double_digit({digit, index}) when Integer.is_odd(index) do
    digit * 2
  end
  defp double_digit({digit, _}), do: digit

  defp sum_double_number([a]),    do: a
  defp sum_double_number([a, b]), do: a + b

  defp modulo(num), do: rem(num, 10)

  defp to_list_of_integers(string) do
    string
    |> String.to_integer
    |> Integer.digits
  end
end
