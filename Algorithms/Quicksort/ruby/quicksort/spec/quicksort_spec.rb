require 'spec_helper'

describe Sort::Quicksort do

  it 'does not sort empty array' do
    q = Sort::Quicksort.new
    expect(q.sort([])).to eq([])
  end

  it 'returns the same one element array' do
    q = Sort::Quicksort.new
    expect(q.sort([2])).to eq([2])
  end

  it 'sorts [3,2,1]' do
    q = Sort::Quicksort.new
    expect(q.sort([3,2,1])).to eql([1,2,3])
  end

  it 'sorts [4,3,2,1]' do
    q = Sort::Quicksort.new
    expect(q.sort( [4,3,2,1] )).to eq([1,2,3,4])
  end
  it "sorts [10,8,9,7,5,6,4,5,1,2,3]" do
    q = Sort::Quicksort.new
    expect(q.sort( [10,8,9,7,5,6,4,1,2,3] )).to eq([1,2,3,4,5,6,7,8,9,10])
  end

  it 'sorts [1,3,2,4]' do
    q = Sort::Quicksort.new
    expect(q.sort([1,3,2,4])).to eq([1,2,3,4])
  end

  it 'sorts ten thousand numbers' do
    q = Sort::Quicksort.new
    nums = (0..10000).to_a
    nums1 = (0..10000).to_a
    expect(q.sort(nums)).to eq(nums1)
  end
end
