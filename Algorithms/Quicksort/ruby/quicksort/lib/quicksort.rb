module Sort
  class Quicksort
    def initialize
    end

    def sort(arr)
      if arr.length < 2
        arr
      else
        pivot = arr.delete_at(rand(arr.length-1)) # choose random element as a pivot
        less, greater = arr.partition { |el| el <= pivot }
        sort(less) + [pivot] + sort(greater)
      end
    end
  end
end
