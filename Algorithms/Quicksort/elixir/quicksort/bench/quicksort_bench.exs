defmodule QuicksortBench do
  use Benchfella

  @list Enum.to_list(1..1000)

  bench "sort" do
    Quicksort.sort([10, 9, 8, 6, 7, 1, 2, 3, 5, 4])
  end
  bench "sort empty" do
    Quicksort.sort([])
  end
  bench "sort one element" do
    Quicksort.sort([5])
  end
  bench "sort thousand elements" do
    Quicksort.sort(@list)
  end
end
