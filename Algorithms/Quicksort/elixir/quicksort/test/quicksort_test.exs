defmodule QuicksortTest do
  use ExUnit.Case
  doctest Quicksort

  test "returns empty list when empty list is passed" do
    assert Quicksort.sort([]) == []
  end

  test "returns the same list if the list has only one element" do
    assert Quicksort.sort([1]) == [1]
  end

  test "sorts two elements list" do
    assert Quicksort.sort([3,1]) == [1,3]
  end

  test "sorts five elements list" do
    assert Quicksort.sort([5,4,3,1,2]) == [1,2,3,4,5]
  end
end
