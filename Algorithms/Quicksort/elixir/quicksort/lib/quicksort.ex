defmodule Quicksort do

  def sort([]),  do: []
  def sort([h]), do: [h]
  def sort(list) do
    pivot = Enum.random(list)
    list_without_pivot = List.delete(list, pivot)
    {smaller, greater} = Enum.partition(list_without_pivot, fn(x) -> x <= pivot end )
    sort(smaller) ++ [pivot] ++ sort(greater)
  end
end
