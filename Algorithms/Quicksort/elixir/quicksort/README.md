# Quicksort

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add quicksort to your list of dependencies in `mix.exs`:

        def deps do
          [{:quicksort, "~> 0.0.1"}]
        end

  2. Ensure quicksort is started before your application:

        def application do
          [applications: [:quicksort]]
        end

