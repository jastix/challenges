defmodule SelectionTest do
  use ExUnit.Case
  doctest Selection

  test "sorts [2] into [2] with reverse" do
    assert Selection.sort([2]) == [2]
  end
  test "sorts [2,3,1] into [3,2,1] with reverse" do
    assert Selection.sort([2,3,1]) == [3,2,1]
  end
  test "sorts [2,3,1] into [1,2,3]" do
    assert Selection.select([2,3,1]) == [1,2,3]
  end

  test "returns 1 from last" do
    assert Selection.smallest([3,2,1]) == 1
  end
  test "returns 1 from middle" do
    assert Selection.smallest([3,1,2]) == 1
  end
  test "returns 2 from two elements list" do
    assert Selection.smallest([3,2]) == 2
  end
  test "returns 3 from one element list" do
    assert Selection.smallest([3]) == 3
  end
end
