defmodule Selection do

  def sort([]),   do: []
  def sort([h]),  do: [h]
  def sort(list), do: Enum.reverse(select list)

  def select([]),  do: []
  def select([h]), do: [h]
  def select(list) do
    smallest_element = smallest(list)
    [smallest_element] ++ select(list -- [smallest_element])
  end

  def smallest([]),     do: []
  def smallest([h]),    do: h
  def smallest([h, t]), do: smallest([min(h, t)])
  def smallest([h|t]) do
    smallest([min(h, hd t)] ++ (tl t))
  end
end
