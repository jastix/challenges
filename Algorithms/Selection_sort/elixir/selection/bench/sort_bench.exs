defmodule SortBench do
  use Benchfella

  @list Enum.to_list(1..1000)

  bench "smallest" do
    Selection.smallest([10, 9, 8, 6, 7, 1, 2, 3, 5, 4])
  end

  bench "select" do
    Selection.select([10, 9, 8, 6, 7, 1, 2, 3, 5, 4])
  end

  bench "sort" do
    Selection.sort([10, 9, 8, 6, 7, 1, 2, 3, 5, 4])
  end
  bench "sort empty" do
    Selection.sort([])
  end
  bench "sort one element" do
    Selection.sort([5])
  end
  bench "sort thousand elements" do
    Selection.sort(@list)
  end
end
